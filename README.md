# Gradoop4jDTA

Implementation of the a Gradoop4jAPI with a graphical User Interface to effectively allow a user to use Gradoop on a Neo4j Database.

Maven install after cloning this repository (Please Substitute Path)


mvn clean package -Ddir="PATH"



Dependency for development:

<dependencies>
	<dependency>
	 <groupId>de.uni.marburg.GradoopForNeo4j</groupId>
         <artifactId>Gradoop4JGUI</artifactId>
         <version>0.1</version>
	</dependency>
</dependencies>
