package Interface.Neo4j;


public interface Neo4jInterface {

    String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    String CYPHER_TARGET_NODE = "CYPHER_TARGET_NODE";
    String CYPHER_SOURCE_NODE = "CYPHER_SOURCE_NODE";

}
