package Interface.Exceptions;


/**
 * Exceptions for empty Graphs
 */

public class EmptyGraphException extends Exception{

    public EmptyGraphException(String message){
        super(message);
    }

}
