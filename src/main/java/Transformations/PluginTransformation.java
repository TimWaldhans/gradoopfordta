package Transformations;

import GraphData.DatabaseConnection.Neo4jReader;
import GraphData.DatabaseConnection.Neo4jWriter;
import Transformations.Transformation;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.io.Serializable;


/**
 * Interface for possibly built in transformations
 */

public abstract class PluginTransformation extends Transformation implements Serializable{


    /**
     * Associated Database connections, input graph and
     * description to be displayed on the GUI and
     * Seriaialization ID
     *
     */


    // marker class
    // reader interface to execute Cypher Querys
    public transient Neo4jReader reader;
    public transient Neo4jWriter writer;
    // Resulting Graph and its string representation
    public transient  LogicalGraph resultGraph;
    public String continuationPattern;
    public String stringRep;
    public final String description = "";
    private static final long serialVersionUID = 0000000000000000006L;



    public PluginTransformation(){};

    public PluginTransformation(String continuationPattern){
        this.continuationPattern = continuationPattern;
    }

    public Neo4jReader getReader() {
        return reader;
    }

    public void setReader(Neo4jReader reader) {
        this.reader = reader;
    }


    public abstract LogicalGraph execute(LogicalGraph lg);


    public String getDescription() {
        return description;
    }

    public String getResultAsString() {
        return stringRep;
    }


    public Neo4jWriter getWriter() {
        return writer;
    }

    public void setWriter(Neo4jWriter writer) {
        this.writer = writer;
    }

    @Override
    public LogicalGraph getResultGraph() {
        return resultGraph;
    }

    @Override
    public void setResultGraph(LogicalGraph resultGraph) {
        this.resultGraph = resultGraph;
        super.setResultGraph(resultGraph);
    }

    public String getContinuationPattern() {
        return continuationPattern;
    }

    public void setContinuationPattern(String continuationPattern) {
        this.continuationPattern = continuationPattern;
    }

    public abstract String getStringRep();

    public void setStringRep(String stringRep) {
        this.stringRep = stringRep;
    }
}
