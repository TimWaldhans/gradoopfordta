package Transformations.TransformationAlgorithms;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import GUI.Controller.Schedule.TransformationElement;
import GraphData.GraphFileHandler.GraphReader;
import Interface.Neo4j.GradoopConfig;

import GUI.Controller.Components.PanelComponents;
import GraphData.DatabaseConnection.Neo4jReader;
import GraphData.DatabaseConnection.Neo4jWriter;
import Transformations.CustomTransformations.BasicTransformation;
import Transformations.GradoopTransformations.AdvancedTransformation;
import Transformations.PluginTransformation;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * This class represents a Transformation Schedule Element
 */

public class TransformationSchedule implements Serializable {

    /**
     * in- and outputgraphs,
     * Associated Databases and transformation lists
     * and database credentials
     */



    private  transient LogicalGraph baseGraph;
    private  List<TransformationElement> transformations = new LinkedList<>();
    private  transient Neo4jWriter writer;
    private  transient Neo4jReader reader;
    private transient LogicalGraph finalGraph;
    private  String cypherBaseQuery;
    private  String path;
    private String URI;
    private String user;
    private String pw;
    private boolean local;
    private static final long serialVersionUID = 0000000000000000007L;



    private String textRep;

    /**
     * Constructor for a remote Database
     * @param cypherBaseQuery associated Cypher query
     * @param URI Associated remote Database URI
     * @param user Username for DB
     * @param pw Password for DB
     */

    public TransformationSchedule(String cypherBaseQuery, String URI, String user, String pw){
         this.cypherBaseQuery = cypherBaseQuery;
         this.URI = URI;
         this.user = user;
         this.pw = pw;
         this.textRep = "";
         this.local = false;
    }


    /**
     * Constructor for a local Database
     * @param cypherBaseQuery
     * @param path
     */

    public TransformationSchedule(String cypherBaseQuery, String path){
        this.cypherBaseQuery = cypherBaseQuery;
        this.path = path;
        this.textRep = "";
        this.local = true;
    }


    /**
     * Adds a Transformation object to this schedule
     * @param trafo
     */


    public void  addGrafo(TransformationElement trafo){

        this.transformations.add(trafo);
    }

    /**
     * Reads in a Graph from a local database with the associated Match string
     */

    public void loadGraphLocally(){
        try {

            this.reader = new Neo4jReader(path);
            this.baseGraph = reader.readGraph(cypherBaseQuery);
            this.baseGraph = reader.readGraph(cypherBaseQuery);

            GDLEncoder enc = new GDLEncoder(baseGraph.getGraphHead().collect(),
                    baseGraph.getVertices().collect(), baseGraph.getEdges().collect());

            textRep = enc.getGDLString();
        }catch(Exception e){
            PanelComponents.callInformation(e.getMessage());
        }
        reader.close();
    }

    /**
     * Reads in a Graph from a local database with the associated Match string
     */

    public void loadGraphRemotely(){
        try {
            this.reader = new Neo4jReader(URI, user, pw);
            this.baseGraph = reader.readGraph(cypherBaseQuery);

            GDLEncoder enc = new GDLEncoder(baseGraph.getGraphHead().collect(),
                    baseGraph.getVertices().collect(), baseGraph.getEdges().collect());

            textRep = enc.getGDLString();
        }catch(Exception e){
           PanelComponents.callInformation(e.getMessage());
        }
        reader.close();
    }

    /**
     * sets this Inintial Graph in case of a imported Graph
     */

    public void importGraph(String path){


        try {
            this.baseGraph = GraphReader.readGraphGDL(path,new GradoopConfig());

            GDLEncoder enc = new GDLEncoder(baseGraph.getGraphHead().collect(),
                    baseGraph.getVertices().collect(), baseGraph.getEdges().collect());

            textRep = enc.getGDLString();
        }catch(Exception e){
            PanelComponents.callInformation(e.getMessage());
        }

    }

    /**
     * Writes the resulting graph to the associated Database
     */

    public void commit(){
        try{

        }catch(Exception e){
            PanelComponents.callInformation(e.getMessage());
        }
        if(local){
            writer = new Neo4jWriter(path);
            writer.putGraph(baseGraph,finalGraph);
            writer.getWriter().close();
        }else{
            writer = new Neo4jWriter(URI,user,pw);
            writer.putGraph(baseGraph,finalGraph);
            writer.getWriter().close();
        }

    }

    /**
     * Runs all associated Transformations
     */

    public void run(){


        try {

            for (int i = 0; i < transformations.size(); i++) {

                if (i == 0) {

                    if (transformations.get(i).getTransformation() instanceof PluginTransformation) {

                        PanelComponents.callInformation("Executing \" " + transformations.get(i).getName() + " \" ");
                        PluginTransformation trafo =  (PluginTransformation) transformations.get(i).getTransformation();

                        Neo4jReader reader;
                        if(local){
                            reader = new Neo4jReader(this.path);
                            trafo = trafo.getClass().getConstructor(String.class).
                                    newInstance(transformations.get(i).getConstructionString());

                            reader.getConfig().setEnv(baseGraph.getConfig().getExecutionEnvironment());


                            trafo.setWriter(new Neo4jWriter(this.path));
                            trafo.setContinuationPattern(transformations.get(i).getConstructionString());

                            transformations.get(i).executeTransformation(baseGraph);
                            finalGraph = trafo.execute(baseGraph);
                        }else{


                            trafo = trafo.getClass().getConstructor().newInstance();

                            trafo.setContinuationPattern(transformations.get(i).getConstructionString());

                            transformations.get(i).executeTransformation(baseGraph);
                            finalGraph = trafo.execute(baseGraph);
                            finalGraph = trafo.getResultGraph();


                        }
                        continue;

                    }else {

                        finalGraph = transformations.get(i).executeTransformation(baseGraph);
                        PanelComponents.callInformation("Executing \" " + transformations.get(i).getName() + " \" ");
                        continue;
                    }
                }
                if (transformations.get(i).getTransformation() instanceof BasicTransformation) {
                    finalGraph = transformations.get(i).executeTransformation(finalGraph);
                    continue;

                }
                if (transformations.get(i).getTransformation() instanceof AdvancedTransformation) {
                    finalGraph = transformations.get(i).executeTransformation(finalGraph);
                    PanelComponents.callInformation("Executing \" " + transformations.get(i).getName() + " \" ");
                    continue;

                }
                if (transformations.get(i).getTransformation() instanceof PluginTransformation) {

                    PanelComponents.callInformation("Executing \" " + transformations.get(i).getName() + " \" ");
                    PluginTransformation trafo =  (PluginTransformation) transformations.get(i).getTransformation();

                    Neo4jReader reader;
                    if(local){
                        reader = new Neo4jReader(this.path);
                        trafo = trafo.getClass().getConstructor(String.class).
                                newInstance(transformations.get(i).getConstructionString());
                        reader.getConfig().setEnv(baseGraph.getConfig().getExecutionEnvironment());
                        trafo.setReader(reader);
                        trafo.setWriter(new Neo4jWriter(this.path));
                        trafo.setContinuationPattern(transformations.get(i).getConstructionString());

                        transformations.get(i).executeTransformation(finalGraph);
                        finalGraph = trafo.execute(finalGraph);
                        finalGraph = trafo.getResultGraph();
                    }else{
                        reader = new Neo4jReader(this.URI,this.user,this.pw);
                        trafo = trafo.getClass().getConstructor(String.class).
                                newInstance(transformations.get(i).getConstructionString());
                        reader.getConfig().setEnv(baseGraph.getConfig().getExecutionEnvironment());
                        trafo.setReader(reader);
                        trafo.setWriter(new Neo4jWriter(this.URI,this.user,this.pw));
                        trafo.setContinuationPattern(transformations.get(i).getConstructionString());

                        transformations.get(i).executeTransformation(finalGraph);
                        finalGraph = trafo.execute(finalGraph);
                        finalGraph = trafo.getResultGraph();

                    }
                    continue;

                }


            }

        }catch (Exception e ){
            PanelComponents.callInformation(e.getMessage());
            e.printStackTrace();
        }


    }



    public String getString(){
        if(baseGraph == null){
            return "";
        }else{
            return textRep;
        }
    }

    public void resetGrafos(){
        this.transformations = new LinkedList<>();
    }



    public String getCypherBaseQuery() {
        if(cypherBaseQuery == null){
            return "";
        }
        return cypherBaseQuery;
    }

    public List<TransformationElement> getTransformations() {
        return transformations;
    }

    public LogicalGraph getfinalGraph() {
        return finalGraph;
    }


    public void setCypherBaseQuery(String cypherBaseQuery) {
        this.cypherBaseQuery = cypherBaseQuery;
    }

    public LogicalGraph getBaseGraph() {
        return baseGraph;
    }


    public void setTransformations(List<TransformationElement> transformations) {
        this.transformations = transformations;
    }



    public void setWriter(Neo4jWriter writer) {
        this.writer = writer;
    }

    public Neo4jReader getReader() {
        return reader;
    }

    public void setReader(Neo4jReader reader) {
        this.reader = reader;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public void setURI(String URI) {
        this.URI = URI;
    }

    public void setBaseGraph(LogicalGraph baseGraph) {
        this.baseGraph = baseGraph;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }





    public void setPw(String pw) {
        this.pw = pw;
    }
}
