package Transformations.TransformationAlgorithms;

import GUI.Controller.Components.PanelComponents;
import Transformations.GradoopTransformations.*;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.io.Serializable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * String handler class for extracting structural
 * Changes from a passed GDL match and construction pattern
 * for making it possible to create consinstent graph structures
 */
public class StringUtils implements Serializable {


    /**
     * Passed GDL patterns and serialization ID
     */
    private String queryPattern;
    private String constructionPattern;
    private static final long serialVersionUID = 8L;

    public StringUtils(String queryPattern, String constructionPattern) {
        this.queryPattern = queryPattern;
        this.constructionPattern = constructionPattern;
    }
    public StringUtils() {

    }


    /**
     * Extracts a set of variables from a given GDL pattern
     * to find their matching ID's in a Graph head
     * @param heads The Graph head of a Graph associated with the pattern
     * @param var The variable to get the ID's for
     * @return List of ID's for a variable
     */

    public List<String> getIdSForPatternVariable(List<EPGMGraphHead> heads, String var) {


        List<String> result = new LinkedList<>();
        for(EPGMGraphHead head :heads){
            PropertyValue values = head.getPropertyValue("__variable_mapping");
            String[] mapping = values.toString().split(",");
            for(String s: mapping){

                String tmp = s.replaceFirst(" ","");
                String[] vals = tmp.split("=");
                if(vals[0].replaceAll("\\{","").equals(var)){
                    result.add(vals[1].replaceAll("\\}",""));
                }

            }

        }

        return result;


    }


    /**
     * Generates a Property Object for a GDL String element
     * e.g. : (n:Person {name: 'Alice'}) -> new Property(name,'Alice')
     * @param element The GDL String
     * @return The generated Property object
     */

    public Properties generatePropertiesFromElement(String element){

        String nodeRep = "\\{(.*?)\\}";
        Pattern pattern = Pattern.compile(nodeRep);
        Matcher matcher = pattern.matcher(element);
        String[] propString = {};
        while (matcher.find()) {
            propString = matcher.group().split(",");

        }
        for(int i = 0;i<propString.length;i++){
            propString[i] = propString[i].replaceFirst("\\{","");
            int pos = propString[i].lastIndexOf("}");
            if (pos > -1) {
                propString[i] = propString[i].substring(0, pos)
                        + ""
                        + propString[i].substring(pos + 1);
            }

        }
        Map<String,Object> propMap = new HashMap<>();
        for(int i = 0;i<propString.length;i++) {

            String[] tmp = propString[i].split(":");
            if(tmp[1].replaceFirst(" ","").charAt(0)== '\"') {
                tmp[1] = tmp[1].replaceFirst("\"", "");
                int pos = tmp[1].lastIndexOf("\"");
                if (pos > -1) {
                    tmp[1] = tmp[1].substring(0, pos)
                            + ""
                            + tmp[1].substring(pos + 1);
                }
                propMap.put(tmp[0],tmp[1]);
            }else{
                propMap.put(tmp[0].replaceFirst(" ",""),Integer.valueOf(tmp[1].replaceAll(" ","")));
            }


        }

        return Properties.createFromMap(propMap);
    }


    /**
     * Extracts a Label String for a GDL String element
     * e.g. : (n:Person {name: 'Alice'}) -> 'Person'
     * @param element The GDL String
     * @return The generated Label
     */

    public String generateLabelFromElement(String element){


            String[] split1 = element.split(":", 2);
            String[] split2 = split1[1].split("\\{", 2);
            return split2[0].replaceFirst(" ","");

    }


    /**
     * Workaround for Gradoops GDL language for
     * consistent Graph results.
     * Checks how the elements in the associated construction and
     * matching patterns are connected and marks
     * the new element with "Target" or "source" or source in the last arrays index of
     * the indices in the list
     * @return List of GDL element variables and how to connect them
     */


    public List<String[]> getElementConnectionBuildPatterns(){

        List<String[]> matches = getGDLPatterns(queryPattern);
        List<String[]> constructionPatterns = getGDLPatterns(constructionPattern);
        List<String[]> result  = new LinkedList<>();

        Set<String> allMatchVars = new HashSet<>();

        for(String[] match2 :matches){
            List<String> m = getVarset(match2);

            m.forEach(ma->allMatchVars.add(ma));

        }

        //newElements
        for(String[] match : matches) {


            inner:
            for (String[] construction : constructionPatterns) {

                List<String> matchVarset = getVarset(match);
                List<String> constructionVarset = getVarset(construction);
                if (construction.length == 3 &&
                        constructionVarset.get(0).equals(matchVarset.get(0))) {
                    if (match.length == 3 && constructionVarset.get(2).equals(matchVarset.get(2))) {
                        continue;
                    }
                    if (patternContained(construction)) {
                        break;
                    }
                    for (String[] match2 : matches) {
                        List<String> matchVarset2 = getVarset(match2);
                        if (matchVarset2.contains(constructionVarset.get(2))) {
                            break inner;
                        }
                    }

                    String[] targetNodeAdded = new String[4];
                    targetNodeAdded[0] = construction[0];
                    targetNodeAdded[1] = construction[1];
                    targetNodeAdded[2] = construction[2];
                    targetNodeAdded[3] = "TARGET";

                    result.add(targetNodeAdded);
                    continue;

                }
                if (construction.length == 3 &&
                        constructionVarset.get(2).equals(matchVarset.get(0))) {

                    if (match.length == 3 && constructionVarset.get(0).equals(matchVarset.get(0))) {
                        continue;
                    }

                    if (patternContained(construction)) {
                        break;
                    }
                    for (String[] match2 : matches) {
                        List<String> matchVarset2 = getVarset(match2);
                        if (matchVarset2.contains(constructionVarset.get(0))) {
                            break inner;
                        }
                    }

                    String[] sourceNodeAdded = new String[4];
                    sourceNodeAdded[0] = construction[0];
                    sourceNodeAdded[1] = construction[1];
                    sourceNodeAdded[2] = construction[2];
                    sourceNodeAdded[3] = "SOURCE";

                    result.add(sourceNodeAdded);
                    continue;
                }
            }
        }
            for(String[] match : matches) {


                inner:
                for (String[] construction : constructionPatterns) {

                    List<String> constructionVarset = getVarset(construction);

                    if (construction.length == 3 && match.length == 3) {

                        String edgeVar = constructionVarset.get(1);

                        if (allMatchVars.contains(edgeVar)) {
                            continue;
                        }

                        String[] connected = new String[4];
                        connected[0] = construction[0];
                        connected[1] = construction[1];
                        connected[2] = construction[2];
                        connected[3] = "CONNECT";
                        result.add(connected);
                    }
                    if (construction.length == 3 && match.length == 1) {
                        String edgeVar = constructionVarset.get(1);

                        if (allMatchVars.contains(edgeVar)) {
                            continue;
                        }
                        if (construction[0].equals(match[0]) || construction[2].equals(match[0])) {
                            String[] connected = new String[4];
                            connected[0] = construction[0];
                            connected[1] = construction[1];
                            connected[2] = construction[2];
                            connected[3] = "CONNECT";
                            result.add(connected);
                        }


                    }

                }
            }



        return  result;

    }


    /**
     * Checks wether a construction pattern is contained in the match
     * Pattern to differentiate between new and old patterns
     * @param construction The List of construction variables
     * @return True, if pattern contained in match string
     */


    public boolean patternContained(String[] construction){

        List<String[]> matches = getGDLPatterns(queryPattern);

        boolean contained = false;


            for(String[] match : matches) {
                if(match.length != construction.length){continue;}
                List<String> matchVarset = getVarset(match);
                List<String> constructionVarset = getVarset(construction);

                boolean sourcesEqual = constructionVarset.get(0).equals(matchVarset.get(0));
                boolean targetsEqual =  constructionVarset.get(2).equals(matchVarset.get(2));
                boolean edgesEqual =  constructionVarset.get(1).equals(matchVarset.get(1));

                contained = sourcesEqual && edgesEqual && targetsEqual;
                if(contained){
                    break;
                }

            }

        return  contained;


    }


    /**
     * Extracts build patterns who mark  Elements which are to be created
     * From the associated match- and construction pattern
     * @return List of the to- be built variables
     */



    /**
     * Extracts a set of Functions with their arguments from a Gradoop Function String
     * for the GUI Interface and maps them to a matching Gradoop function
     * @param functionPattern
     * @return
     */

    public HashMap<String,AdvancedTransformation> getGradoopFunctionStrings(String functionPattern){


        String[] exp  = functionPattern.split("\\),");
        HashMap<String,AdvancedTransformation> map = new HashMap<>();

        for(String subexpression :exp) {
            subexpression = subexpression + ")";

            String functionName = subexpression.split("\\(",2)[0];
            String parantheses = "\\((.*?)\\)";
            Pattern pattern2 = Pattern.compile(parantheses);
            Matcher matcher2 = pattern2.matcher(subexpression);

            String arguments = "";
            while (matcher2.find()) {
                String[] expr2 = new String[1];
                expr2[0] = matcher2.group(1);
                arguments = expr2[0];

            }

            functionName = functionName.replace(" ","");
            functionName = functionName.replace("\n","");
            functionName = functionName.toLowerCase();
            if(functionName.equals(")")){break;}
            switch (functionName){
                case "minproperty":
                case "minedgeproperty":
                case "minvertexproperty":
                case "maxproperty":
                case "maxedgeproperty":
                case "maxvertexproperty":
                case "sumproperty":
                case "sumedgeproperty":
                case "sumvertexproperty":
                case "averageproperty":
                case "averageedgeproperty":
                case "averagevertexproperty":
                case "count":
                case "edgecount":
                case "vertexcount":
                case "haslabel":
                case "hasedgelabel":
                case "hasvertexlabel":
                     map.put(functionName,new GradoopAggregation(functionName,arguments));
                     break;
                case "transformvertexproperties":
                case "transformedgeproperties":
                     map.put(functionName,new PropertyTransformationFunc(functionName,arguments));
                     break;
                case "vertextoedge":
                case "connectneighbors":
                case "extractpropertyfromvertex":
                case "propagatepropertytoneighbor":
                case "invertedges":
                     map.put(functionName,new DataIntegration(functionName,arguments));
                     break;
                case "combine":
                case "overlap":
                case "exclude":
                    map.put(functionName,new DualGraphOperation(functionName,arguments));
                    break;
                default:
                    PanelComponents.callInformation("Transformation " + functionName + " not recognized. " +
                            "Please redefine");


            }






        }

        return map;


    }


    public List<String[]> getNewElementBuildPatterns(){

        List<String[]> elementsToConnect = getElementConnectionBuildPatterns();
        List<String[]> elementsToCreate= getGDLPatterns(constructionPattern);
        List<String[]> matchedElements = getGDLPatterns(queryPattern);
        List<String> toOmit = new LinkedList<>();
        List<String[]> result = new LinkedList<>();

        for(String[] s : elementsToConnect){
            toOmit.add(getVar(s[0]));
            toOmit.add(getVar(s[1]));
            toOmit.add(getVar(s[2]));

        }
        for(String[] s : matchedElements){
            if(s.length==1){
                toOmit.add(getVar(s[0]));
            }
            if(s.length==3){
                toOmit.add(getVar(s[0]));
                toOmit.add(getVar(s[1]));
                toOmit.add(getVar(s[2]));
            }
        }
        for(String[] s : elementsToCreate){
            if(toOmit.contains(getVar(s[0]))){continue;}
            if(s.length==3) {
                String[] tmp = {s[0], s[1], s[2], "NEW"};
                result.add(tmp);
            }else{
                String[] tmp = {s[0],"NEW"};
                result.add(tmp);
            }
        }

        return  result;

    }


    public  String getVar(String input ){

        String[] split = input.split(":",2);


        return split[0];

    }


    public  List<String> getVarset(String[] input ){

        List<String> result = new LinkedList<>();
        for(String s : input){
            String[] split = s.split(":",2);
            result.add(split[0]);
        }

        return result;

    }


    public  List<String[]> getGDLPatterns(String expression){

        expression = expression.replaceFirst("MATCH","");
        expression = expression.replaceFirst("Match","");
        expression = expression.replaceFirst("match","");

        String[] exp  = expression.split("\\),");
        List<String[]> result = new LinkedList<>();
        String nodeEdgeRep = "\\((.*?)\\)-+\\[(.*?)]->+\\((.*?)\\)";
        Pattern pattern1 = Pattern.compile(nodeEdgeRep);

        for(String subexpression :exp) {
            String tmp = subexpression + ")";
            Matcher matcher = pattern1.matcher(tmp);
            while (matcher.find()) {
                String[] expr = new String[3];
                expr[0] = matcher.group(1);
                expr[1] = matcher.group(2);
                expr[2] = matcher.group(3);
                result.add(expr);
            }
            if(subexpression.contains("-")&&subexpression.contains("->")&&subexpression.contains("[")
                    &&subexpression.contains("]")){continue;}
            String nodeRep = "\\((.*?)\\)";
            Pattern pattern2 = Pattern.compile(nodeRep);
            Matcher matcher2 = pattern2.matcher(tmp);
            while (matcher2.find()) {
                String[] expr2 = new String[1];
                expr2[0] = matcher2.group(1);
                matcher2.group(1);
                result.add(expr2);
            }



        }



        return  result;

    }




}
