package Transformations.GradoopTransformations;

import GUI.Controller.Components.PanelComponents;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.aggregation.functions.average.AverageEdgeProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.average.AverageProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.average.AverageVertexProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.containment.HasEdgeLabel;
import org.gradoop.flink.model.impl.operators.aggregation.functions.containment.HasLabel;
import org.gradoop.flink.model.impl.operators.aggregation.functions.containment.HasVertexLabel;
import org.gradoop.flink.model.impl.operators.aggregation.functions.count.EdgeCount;
import org.gradoop.flink.model.impl.operators.aggregation.functions.count.VertexCount;
import org.gradoop.flink.model.impl.operators.aggregation.functions.max.MaxEdgeProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.max.MaxProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.max.MaxVertexProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.min.MinEdgeProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.min.MinProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.min.MinVertexProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.sum.SumEdgeProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.sum.SumProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.sum.SumVertexProperty;
import org.gradoop.flink.model.impl.operators.aggregation.functions.count.Count;

import java.io.Serializable;

/**
 * Implements a Gradoop aggregation function
 */


public class GradoopAggregation extends AdvancedTransformation implements Serializable{


    /**
     * Passed arguments for the Property transformation,
     * In- and output Graph, string representation of the output Graph and
     * the serialization ID
     */

    private String arguments;
    private String functionName;
    private transient LogicalGraph inputGraph;
    private transient LogicalGraph resultGraph;
    private String stringRep;
    private static final long serialVersionUID = 12L;


    /**
     * Constructor with function type and arguments
     * @param functionType Function name
     * @param arguments arguments for functions
     */



    public GradoopAggregation (String functionType,String arguments){
        this.arguments = arguments;
        this.functionName = functionType;
    }


    /**
     * Executes the transformation on the input Graph
     * @param lg The input Graph
     * @return The result Graph
     */

    public LogicalGraph execute (LogicalGraph lg){



        String[] args = arguments.split(",");
        inputGraph = lg;
        switch (functionName) {
            case "minproperty":
                minProperty(args);
                break;
            case "minedgeproperty":
                minEdgeProperty(args);
                break;
            case "minvertexproperty":
                minVertexProperty(args);
                break;
            case "maxproperty":
                maxProperty(args);
                break;
            case "maxedgeproperty":
                maxEdgeProperty(args);
                break;
            case "maxvertexproperty":
                maxVertexProperty(args);
                break;
            case "sumproperty":
                sumProperty(args);
                break;
            case "sumedgeproperty":
                sumEdgeProperty(args);
                break;
            case "sumvertexproperty":
                sumVertexProperty(args);
                break;
            case "averageproperty":
                averageProperty(args);
                break;
            case "averageedgeproperty":
                averageEdgeProperty(args);
                break;
            case "averagevertexproperty":
                averageVertexProperty(args);
                break;
            case "count":
                count(args);
                break;
            case "edgecount":
                edgeCount(args);
                break;
            case "vertexcount":
                 vertexCount(args);
                break;
            case "haslabel":
                  hasLabel(args);
                break;
            case "hasedgelabel":
                  hasEdgeLabel(args);
                break;
            case "hasvertexlabel":
                  hasVertexLabel(args);
        }


        return resultGraph;

    }

    // Implementation of the gradoop functionalities and their result Graphs

    private LogicalGraph hasLabel(String[] args){
        try{
            if(args.length==1){

                HasLabel res1 = new HasLabel(args[0]);
                this.resultGraph = inputGraph.aggregate(res1);



            }
            if(args.length==2){
                HasLabel res2 = new HasLabel(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res2);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph hasEdgeLabel(String[] args){
        try{
            if(args.length==1){

                HasEdgeLabel res= new HasEdgeLabel(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                HasEdgeLabel res = new HasEdgeLabel(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph hasVertexLabel(String[] args){
        try{
            if(args.length==1){

                HasVertexLabel res = new HasVertexLabel(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                HasVertexLabel res= new HasVertexLabel(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }



    private LogicalGraph edgeCount(String[] args){

        try{
            if(args.length==1) {

                EdgeCount res = new EdgeCount(args[0]);
                this.resultGraph = inputGraph.aggregate(res);
            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;

    }

    private LogicalGraph vertexCount(String[] args){

        try{
            if(args.length==1) {

                VertexCount res = new VertexCount(args[0]);
                this.resultGraph = inputGraph.aggregate(res);
            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;

    }


    private LogicalGraph count(String[] args){

        try{
            if(args.length==1) {

                Count res = new Count(args[0]);
                this.resultGraph = inputGraph.aggregate(res);
            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;

    }

    private LogicalGraph averageEdgeProperty(String[] args){
        try{
            if(args.length==1){

                AverageEdgeProperty res = new AverageEdgeProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                AverageEdgeProperty minProp = new  AverageEdgeProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(minProp);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph averageVertexProperty(String[] args){
        try{
            if(args.length==1){

                AverageVertexProperty res = new AverageVertexProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                AverageVertexProperty res = new  AverageVertexProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph averageProperty(String[] args){
        try{
            if(args.length==1){

                AverageProperty res = new AverageProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                AverageProperty res= new  AverageProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph sumEdgeProperty(String[] args){
        try{
            if(args.length==1){

                SumEdgeProperty res = new SumEdgeProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                SumEdgeProperty res = new SumEdgeProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph sumVertexProperty(String[] args){
        try{
            if(args.length==1){

                SumVertexProperty res = new SumVertexProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                SumVertexProperty res= new SumVertexProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph sumProperty(String[] args){
        try{
            if(args.length==1){

                SumProperty res = new SumProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                SumProperty res = new SumProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }



    private LogicalGraph maxVertexProperty(String[] args){
        try{
            if(args.length==1){

                MaxVertexProperty res = new MaxVertexProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                MaxVertexProperty res = new MaxVertexProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph maxEdgeProperty(String[] args){
        try{
            if(args.length==1){

                MaxEdgeProperty res= new MaxEdgeProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                MaxEdgeProperty res = new MaxEdgeProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph maxProperty(String[] args){
        try{
            if(args.length==1){

                MaxProperty res = new MaxProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(res);



            }
            if(args.length==2){
                MinProperty res = new MinProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(res);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph minVertexProperty(String[] args){
        try{
            if(args.length==1){

                MinVertexProperty minProp = new MinVertexProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(minProp);



            }
            if(args.length==2){
                MinVertexProperty minProp = new MinVertexProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(minProp);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;
    }

    private LogicalGraph minEdgeProperty(String[] args){

        try{
            if(args.length==1){

                MinEdgeProperty minEdgeProp = new MinEdgeProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(minEdgeProp);



            }
            if(args.length==2){
                MinEdgeProperty minProp = new MinEdgeProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(minProp);;

            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }




        return this.resultGraph;


    }


    private LogicalGraph minProperty(String[] args){


        try{
            if(args.length==1){


                MinProperty minProp = new MinProperty(args[0]);
                this.resultGraph = inputGraph.aggregate(minProp);

                inputGraph.print();

            }
            if(args.length==2){
                MinProperty minProp = new MinProperty(args[0],args[1]);

                this.resultGraph = inputGraph.aggregate(minProp);;

            }


            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch (Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }

        return this.resultGraph;


    }


    @Override
    public String getResultAsString() {

        return stringRep;



    }
}
