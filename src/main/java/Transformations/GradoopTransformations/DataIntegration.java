package Transformations.GradoopTransformations;

import GUI.Controller.Components.PanelComponents;
import org.gradoop.dataintegration.transformation.ConnectNeighbors;
import org.gradoop.dataintegration.transformation.PropagatePropertyToNeighbor;
import org.gradoop.dataintegration.transformation.VertexToEdge;
import org.gradoop.dataintegration.transformation.impl.ExtractPropertyFromVertex;
import org.gradoop.dataintegration.transformation.impl.InvertEdges;
import org.gradoop.dataintegration.transformation.impl.Neighborhood;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;



/**
 * Implements a Data integration  operation function
 */

public class DataIntegration extends AdvancedTransformation {


    /**
     * Passed arguments for the Property transformation,
     * In- and output Graph, string representation of the output Graph and
     * the serialization ID
     */
    private String arguments;
    private String functionName;
    private transient LogicalGraph inputGraph;
    private transient LogicalGraph resultGraph;
    private String stringRep;
    private static final long serialVersionUID = 14L;



    /**
     * Constructor with function type and arguments
     * @param functionType Function name
     * @param arguments arguments for functions
     */


    public DataIntegration(String functionType, String arguments){
        this.arguments = arguments;
        this.functionName = functionType;
    }

    /**
     * Executes the transformation on the input Graph
     * @param lg The input Graph
     * @return The result Graph
     */


    public LogicalGraph execute (LogicalGraph lg){



        String[] args = arguments.split(",");
        inputGraph = lg;

        switch (functionName) {

            case "vertextoedge":
                vertexToEdge(args);
                break;
            case "connectneighbors":
                connectneighbors(args);
                break;
            case "extractpropertyfromvertex":
                extractpropertyfromvertex(args);
                break;
            case "propagatepropertytoneighbor":
                propagatepropertytoneighbor(args);
                break;
            case "invertedges":
                invertedges(args);
                break;
        }



        return resultGraph;

    }


    // Implementation of the Gradoop functionalities

    private void  vertexToEdge(String[] args){
        try {

            if(args.length==2) {
                VertexToEdge vertexToEdge = new VertexToEdge(args[0], args[1]);
                resultGraph = inputGraph.callForGraph(vertexToEdge);

                GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                        this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
                stringRep = enc.getGDLString();
            }

        }catch(Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }
    }

    private void  connectneighbors(String[] args){
        try {

            if(args.length==4) {
                Neighborhood.EdgeDirection ed;
                switch (args[1].toLowerCase()){

                    case "incoming":
                        ed = Neighborhood.EdgeDirection.INCOMING;
                        break;
                    case "outgiong":
                        ed = Neighborhood.EdgeDirection.OUTGOING;
                        break;
                    case "undirected":
                        ed = Neighborhood.EdgeDirection.UNDIRECTED;
                        break;
                        default:
                            ed = Neighborhood.EdgeDirection.UNDIRECTED;

                }


                UnaryGraphToGraphOperator connectNeighbors = new ConnectNeighbors(
                        args[0], ed,
                        args[2], args[3]);
                resultGraph= inputGraph.callForGraph(connectNeighbors);
            }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

        }catch(Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }
    }


    private void  extractpropertyfromvertex(String[] args){
        try {

            if(args.length==4) {

                ExtractPropertyFromVertex extract = new ExtractPropertyFromVertex(
                        args[0], args[1],
                        args[2], args[3]);
                resultGraph = inputGraph.callForGraph(extract);

                GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                        this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
                stringRep = enc.getGDLString();
            }


        }catch(Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }
    }

    private void  propagatepropertytoneighbor(String[] args){
        try {

           if(args.length==3) {

               PropagatePropertyToNeighbor p2n= new PropagatePropertyToNeighbor
                       (args[0], args[1], args[2]);
               resultGraph = inputGraph.callForGraph(p2n);

               GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                       this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
               stringRep = enc.getGDLString();

           }

        }catch(Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }
    }

    private void  invertedges(String[] args){
        try {

            if (args.length == 2) {
                InvertEdges invertEdges = new InvertEdges(args[0], args[1]);
                resultGraph = inputGraph.transformEdges(invertEdges);

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();
        }
        }catch(Exception e){
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for "+ functionName);
        }
    }



    @Override
    public String getResultAsString() {

        return stringRep;



    }
}
