package Transformations.GradoopTransformations;

import GraphData.DatabaseConnection.Neo4jReader;
import Transformations.Transformation;
import Transformations.TransformationAlgorithms.StringUtils;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;


import java.io.Serializable;
import java.util.HashMap;

import java.util.Map;

public class AdvancedTransformation extends Transformation implements Serializable {


    private final String queryPattern;
    private final String constructionPattern;
    private transient LogicalGraph inputgraph;
    private transient LogicalGraph resultGraph;
    private String stringRep;
    private StringUtils utils;
    private static final long serialVersionUID = 15L;



    public AdvancedTransformation(){
        this.queryPattern = "";
        this.constructionPattern = "";

    }

    public AdvancedTransformation(String queryPattern, String constructionPattern) {
        super(queryPattern,constructionPattern);
        this.utils = new StringUtils(queryPattern,constructionPattern);
        this.queryPattern = queryPattern;
        this.constructionPattern = constructionPattern;
        this.utils = new StringUtils();
    }

    public LogicalGraph execute(LogicalGraph lg){

        inputgraph = lg;


        try {
         GraphCollection gc = lg.query(queryPattern);

         ReduceCombination rc = new ReduceCombination();
         resultGraph = (LogicalGraph) rc.execute(gc);
         HashMap<String,AdvancedTransformation> trafos = utils.getGradoopFunctionStrings(constructionPattern);


         for(Map.Entry e : trafos.entrySet()){


             if(e.getValue() instanceof  GradoopAggregation){

                 GradoopAggregation ag = (GradoopAggregation) e.getValue();

                 this.resultGraph = ag.execute(resultGraph);


             }
             if(e.getValue() instanceof DataIntegration){

                 DataIntegration f = (DataIntegration) e.getValue();

                 this.resultGraph = f.execute(resultGraph);


             }
             if(e.getValue() instanceof  PropertyTransformationFunc){


                 PropertyTransformationFunc f = (PropertyTransformationFunc) e.getValue();

                 this.resultGraph = f.execute(resultGraph);


             }
             if(e.getValue() instanceof  DualGraphOperation){


                 DualGraphOperation f = (DualGraphOperation) e.getValue();

                 this.resultGraph = f.execute(inputgraph);


             }

         }

            GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                    this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();


            return resultGraph;

        }catch(Exception e){
            e.printStackTrace();
        }

        return resultGraph;

    }


    public LogicalGraph execute(String matchPattern, String constructionPattern,
                                Neo4jReader reader){
        return execute(inputgraph);
    }


    @Override
    public LogicalGraph getResultGraph() {
        return resultGraph;
    }

    @Override
    public String getResultAsString() {
        try {

            return stringRep;

        }catch(Exception e){
            e.printStackTrace();
        }

        return "";
    }




}
