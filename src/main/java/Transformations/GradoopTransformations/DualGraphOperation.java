package Transformations.GradoopTransformations;

import org.gradoop.flink.model.impl.epgm.LogicalGraph;


/**
 * Implements a Dual Graph operation function
 */
public class DualGraphOperation extends AdvancedTransformation {


    /**
     * Passed arguments for the Property transformation,
     * In- and output Graph, string representation of the output Graph and
     * the serialization ID
     */

    private String arguments;
    private String functionName;
    private LogicalGraph inputGraph;
    private LogicalGraph resultGraph;
    private String stringRep;
    private static final long serialVersionUID = 13L;


    /**
     * Constructor with function type and arguments
     * @param functionType Function name
     * @param arguments arguments for functions
     */


    public DualGraphOperation (String functionType,String arguments){
        this.arguments = arguments;
        this.functionName = functionType;
    }

    /**
     * Executes the transformation on the input Graph
     * @param lg The input Graph
     * @return The result Graph
     */

    public LogicalGraph execute (LogicalGraph lg){

        inputGraph = lg;

        switch (functionName) {
            case "combine":
                return inputGraph.combine(lg);
            case "overlap":
                return inputGraph.overlap(lg);
            case "exclude":
                return inputGraph.exclude(lg);
        }


        return resultGraph;

    }



}
