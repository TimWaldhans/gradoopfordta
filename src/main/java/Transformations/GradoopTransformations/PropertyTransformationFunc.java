package Transformations.GradoopTransformations;

import GUI.Controller.Components.PanelComponents;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;

import java.util.List;


/**
 * Implements a Gradoop Function to transform Vertex- or Edge Properties
 */

public class PropertyTransformationFunc extends AdvancedTransformation {


    /**
     * Passed arguments for the Property transformation,
     * In- and output Graph, string representation of the output Graph and
     * the serialization ID
     */


    private String arguments;
    private String functionName;
    private LogicalGraph inputGraph;
    private LogicalGraph resultGraph;
    private String stringRep;
    private static final long serialVersionUID = 11L;


    /**
     * Constructor with function type and arguments
     * @param functionType Function name
     * @param arguments arguments for functions
     */



    public PropertyTransformationFunc (String functionType,String arguments){
        this.arguments = arguments;
        this.functionName = functionType;
    }


    /**
     * Executes the transformation on the input Graph
     * @param lg The input Graph
     * @return The result Graph
     */

    public LogicalGraph execute (LogicalGraph lg){



        String[] args = arguments.split(",");
        inputGraph = lg;

        switch (functionName) {
            case "transformvertexproperties":
                this.resultGraph = transformVertexProperties(args);
                break;
            case "transformedgeproperties":
                this.resultGraph = transformEdgeProperties(args);
        }
        return resultGraph;

    }


    /**
     * Implements the Vertex Property Transformation
     * @param args Arguments for the functions
     * @return Resulting Graph
     */

    private LogicalGraph transformVertexProperties(String[] args){
        try {
            if (args.length == 3 ) {

                List<EPGMEdge> resultEdges = inputGraph.getEdges().collect();
                List<EPGMVertex> resultVertices = inputGraph.getVertices().collect();

                try {
                    if (!args[2].startsWith("\"")) {
                        for (EPGMVertex v : resultVertices) {
                            if (v.getLabel().equals(args[0])) {
                                v.setProperty(args[1], getNumericValueFromString(args[2]));
                            }
                        }
                    } else {
                        for (EPGMVertex v : resultVertices) {
                            args[2] = args[2].replace("\"","");
                            if (v.getLabel().equals(args[0])) {
                                if(args[2].equals("null")){
                                   v.removeProperty(args[1]);
                                   continue;
                                }
                                v.setProperty(args[1], args[2]);
                            }
                        }
                    }
                    LogicalGraphFactory graphFac = inputGraph.getConfig().getLogicalGraphFactory();
                    LogicalGraph result = graphFac.fromCollections(inputGraph.getGraphHead().collect().get(0),
                            resultVertices,
                            resultEdges);

                    this.resultGraph = result;


                    GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                            this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
                    stringRep = enc.getGDLString();

                    resultGraph.print();
                    System.out.println(stringRep);

                    return this.resultGraph;
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for " + functionName);
        }

        return null;
    }

    /**
     * Implements the Edge Property Transformation
     * @param args Arguments for the functions
     * @return Resulting Graph
     */

    private LogicalGraph transformEdgeProperties(String[] args) {
        try {

            if (args.length == 3 ) {

                List<EPGMEdge> resultEdges = inputGraph.getEdges().collect();
                List<EPGMVertex> resultVertices = inputGraph.getVertices().collect();

                try {
                    if (!args[2].startsWith("\"")) { ;
                        for (EPGMEdge v : resultEdges) {
                            if (v.getLabel().equals(args[0])) {
                                v.setProperty(args[1], getNumericValueFromString(args[2]));
                            }
                        }
                    } else {
                        for (EPGMEdge v : resultEdges) {
                            args[2] = args[2].replace("\"", "");
                            if (v.getLabel().equals(args[0])) {
                                if (args[2].equals("null")) {
                                    v.removeProperty(args[1]);
                                    continue;
                                }
                                v.setProperty(args[1], args[2]);
                            }
                        }
                    }
                    LogicalGraphFactory graphFac = inputGraph.getConfig().getLogicalGraphFactory();
                    LogicalGraph result = graphFac.fromCollections(inputGraph.getGraphHead().collect().get(0),
                            resultVertices, resultEdges);



                    this.resultGraph = result;

                    GDLEncoder enc = new GDLEncoder(this.resultGraph.getGraphHead().collect(),
                            this.resultGraph.getVertices().collect(), this.resultGraph.getEdges().collect());
                    stringRep = enc.getGDLString();

                    return this.resultGraph;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            PanelComponents.callInformation("Mismatched arguments or values for " + functionName);
        }

        return this.resultGraph;
    }

    /**
     * If necsessary, converts a string to its numeric value
     * @param s String argument
     * @return the matching numeric value
     */

    private Number getNumericValueFromString(String s){
        if(s.contains(".")){
            return Double.parseDouble(s);
        }else{
            return Long.parseLong(s);
        }

    }


    @Override
    public String getResultAsString() {
;
        return stringRep;

    }









}
