package Transformations;


import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.io.Serializable;

/**
 * Interface for Serialization and handling
 * several transformations in a Transformation schedule
 */

public abstract class Transformation implements Serializable{


    /**
     * Ingoing patterns,
     * the resulting Graph and a unique ID for serialization
     */

    private String matchPattern;
    private String constructionPattern;
    private transient LogicalGraph resultGraph;
    private static final long serialVersionUID = 0000000000000000005L;


    /**
     * Constructor for empty Transformation
     */


    public Transformation(){}

    /**
     * Constructor for a Transformation, with match- and construction Pattern
     */

    public Transformation(String matchPattern, String constructionPattern){

        this.matchPattern = matchPattern;
        this.constructionPattern = constructionPattern;

    }

    /**
     * Execution function
     * @param lg The input Graph this transformation is executed on
     * @return The result graph
     */

    public abstract LogicalGraph execute(LogicalGraph lg);

    public LogicalGraph getResultGraph(){
        return resultGraph;
    }

    public void setResultGraph(LogicalGraph resultGraph) {
        this.resultGraph = resultGraph;
    }

    public abstract String getResultAsString();





}
