package Transformations.CustomTransformations;


import GraphData.DatabaseConnection.Neo4jReader;
import Transformations.TransformationAlgorithms.StringUtils;
import Transformations.Transformation;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.operators.base.BinaryCollectionToCollectionOperatorBase;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;
import org.gradoop.flink.model.impl.operators.matching.common.MatchStrategy;
import org.gradoop.flink.model.impl.operators.matching.common.statistics.GraphStatistics;
import org.gradoop.flink.model.impl.operators.overlap.ReduceOverlap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Class for a basic GDL Pattern transformations
 */

public class BasicTransformation extends Transformation implements Serializable {

    /**
     * Associated query - and construction patterns,
     * in -and outputgraphs, String utils for GDL patterns,
     * The result Graphs string representation
     * and Serialization ID
     */

    private final String queryPattern;
    private final String constructionPattern;
    private transient LogicalGraph inputgraph;
    private  StringUtils utils;
    private transient LogicalGraph resultGraph;
    private String stringRep;
    private static final long serialVersionUID = 16L;


    /**
     * Empty constructor
     */


    public BasicTransformation(){
        this.queryPattern = "";
        this.constructionPattern = "";

    }

    /**
     * Constructor with associated Patterns
     * @param queryPattern
     * @param constructionPattern
     */


    public BasicTransformation(String queryPattern, String constructionPattern) {
        super(queryPattern,constructionPattern);
        this.utils = new StringUtils(queryPattern,constructionPattern);
        this.queryPattern = queryPattern;
        this.constructionPattern = constructionPattern;
    }

    /**
     * Executes this Transformations with the string utils and
     * GDL workarounds to generate a consistent graph.
     * E.G. the connection of newly created GDL Nodes are built with
     * matchin source- and target ID'S so that there are no null nodes
     * @param lg The input Graph this transformation is executed on
     * @return
     */


    public LogicalGraph execute(LogicalGraph lg ){

        inputgraph = lg;

        try {
            long a = lg.getVertices().count();
            long b = lg.getEdges().count();
            GraphCollection gc = lg.query(
                    queryPattern,
                    constructionPattern,true,
                    MatchStrategy.HOMOMORPHISM,
                    MatchStrategy.HOMOMORPHISM,
                    new GraphStatistics(a,b,a,a));

            ReduceCombination rc = new ReduceCombination();
            //ReduceOverlap ro = new ReduceOverlap();
            // Duplikatselemente
            LogicalGraph tmpResult = (LogicalGraph) rc.execute(gc);

                     List<EPGMEdge> resultEdges = tmpResult.getEdges().collect();
            List<EPGMVertex> resultVertices = tmpResult.getVertices().collect();
            HashMap<GradoopId,Boolean> asSourceAdded = new HashMap<>();
            HashMap<GradoopId,Boolean> asTargetAdded = new HashMap<>();
            HashMap<GradoopId,Boolean> edgeAdded = new HashMap<>();


            List<String[]> elementsToConnect = utils.getElementConnectionBuildPatterns();
            List<String[]> newElements = utils.getNewElementBuildPatterns();
            newElements.forEach(e-> elementsToConnect.add(e));
            for(String[] elements : elementsToConnect) {

                if(elements.length != 4){

                    continue;

                }

                String sourceNode = elements[0];
                String edge = elements[1];
                String targetNode = elements[2];
                String type = elements[3];

                for (EPGMEdge e : resultEdges) {
                    for (EPGMVertex v : resultVertices) {
                        if(edgeAdded.get(e.getId())!=null){break;}
                        if(asSourceAdded.get(v.getId())!=null &&  asTargetAdded.get(v.getId())!=null){continue;}

                        if((type.equals("SOURCE")) & (e.getLabel().equals(utils.generateLabelFromElement(edge)))) {
                            try {
                                if (v.getLabel().equals(utils.generateLabelFromElement(sourceNode))) {
                                    asSourceAdded.put(v.getId(), true);
                                    e.setSourceId(v.getId());
                                    edgeAdded.put(e.getId(), true);
                                    e.setProperties(utils.generatePropertiesFromElement(edge));
                                    v.setProperties(utils.generatePropertiesFromElement(sourceNode));
                                    continue;
                                }
                            }catch(Exception ex){
                                asSourceAdded.put(v.getId(), true);
                                e.setSourceId(v.getId());
                                edgeAdded.put(e.getId(), true);
                                e.setProperties(utils.generatePropertiesFromElement(edge));
                                v.setProperties(utils.generatePropertiesFromElement(sourceNode));
                            }
                        }
                        if (type.equals("TARGET") & e.getLabel().equals(utils.generateLabelFromElement(edge))) {
                            try {
                                if (v.getLabel().equals(utils.generateLabelFromElement(targetNode))) {

                                    e.setTargetId(v.getId());
                                    e.setProperties(utils.generatePropertiesFromElement(edge));
                                    v.setProperties(utils.generatePropertiesFromElement(targetNode));
                                    asTargetAdded.put(v.getId(), true);
                                    edgeAdded.put(e.getId(), true);
                                    continue;
                                }
                            }catch(Exception ex){
                                e.setTargetId(v.getId());
                                e.setProperties(utils.generatePropertiesFromElement(edge));
                                v.setProperties(utils.generatePropertiesFromElement(targetNode));
                                asTargetAdded.put(v.getId(), true);
                                edgeAdded.put(e.getId(), true);
                            }
                        }
                        if (type.equals("NEW") & e.getLabel().equals(utils.generateLabelFromElement(edge))) {

                            if(asTargetAdded.get(v.getId())!=null || asTargetAdded.get(e.getId())!=null){continue;}
                            try {
                                if (v.getLabel().equals(utils.generateLabelFromElement(targetNode))) {

                                    asTargetAdded.put(e.getId(), true);
                                    e.setTargetId(v.getId());
                                    e.setProperties(utils.generatePropertiesFromElement(edge));
                                    v.setProperties(utils.generatePropertiesFromElement(targetNode));
                                    asTargetAdded.put(v.getId(), true);
                                }
                            }catch(Exception ex){
                            e.setTargetId(v.getId());
                            e.setProperties(utils.generatePropertiesFromElement(edge));
                            v.setProperties(utils.generatePropertiesFromElement(targetNode));
                            asTargetAdded.put(v.getId(), true);
                            edgeAdded.put(e.getId(), true);
                            }
                            if(asSourceAdded.get(v.getId())!=null || asSourceAdded.get(e.getId()) != null){continue;}
                            try {
                                if (v.getLabel().equals(utils.generateLabelFromElement(sourceNode))) {

                                    asSourceAdded.put(e.getId(), true);
                                    asSourceAdded.put(v.getId(), true);
                                    e.setSourceId(v.getId());
                                    e.setProperties(utils.generatePropertiesFromElement(edge));
                                    v.setProperties(utils.generatePropertiesFromElement(sourceNode));
                                }
                            }catch(Exception ex){
                            e.setTargetId(v.getId());
                            e.setProperties(utils.generatePropertiesFromElement(edge));
                            v.setProperties(utils.generatePropertiesFromElement(targetNode));
                            asTargetAdded.put(v.getId(), true);
                            edgeAdded.put(e.getId(), true);
                            }

                        }
                        if(e.getLabel().equals(utils.generateLabelFromElement(edge)) && type.equals("CONNECT")){
                            if(e.hasProperty("CYPHER_SOURCE_ID")){
                                continue;
                            }
                            e.setProperties(utils.generatePropertiesFromElement(edge));

                        }
                    }
                }
            }
           asTargetAdded = new HashMap<>();
            for(String[] elements : elementsToConnect) {
                if(elements.length == 2){
                    Properties p  = utils.generatePropertiesFromElement(elements[0]);
                    String label = utils.generateLabelFromElement(elements[0]);
                    for(EPGMVertex v : resultVertices){
                        if(asTargetAdded.get(v.getId())!=null){continue;}
                        if(v.getLabel().equals(label)) {
                            asTargetAdded.put(v.getId(),true);
                            v.setProperties(p);
                        }
                    }

                }
            }





            LogicalGraphFactory graphFac = tmpResult.getConfig().getLogicalGraphFactory();
            LogicalGraph result = graphFac.fromCollections(resultVertices, resultEdges);;
            this.resultGraph = result;


            GDLEncoder enc = new GDLEncoder(resultGraph.getGraphHead().collect(),
                    resultGraph.getVertices().collect(), resultGraph.getEdges().collect());
            stringRep = enc.getGDLString();

            return result;

        }catch(Exception e){
                e.printStackTrace();
        }

        return null;

    }


    public LogicalGraph execute(){
        return execute(inputgraph);
    }

    public LogicalGraph execute(String matchPattern, String constructionPattern,
                                Neo4jReader reader){
        return execute(inputgraph);
    }


    public void setInputgraph(LogicalGraph inputgraph) {
        this.inputgraph = inputgraph;
    }

    public void setStringRep(String stringRep) {
        this.stringRep = stringRep;
    }


    public String getStringRep() {
        return stringRep;
    }

    public LogicalGraph getResultGraph() {return  resultGraph;}

    public void setResultGraph(LogicalGraph resultGraph) {
        this.resultGraph = resultGraph;
    }


    @Override
    public String getResultAsString() {
        try {

            return stringRep;

        }catch(Exception e){
            e.printStackTrace();
        }

        return "";
    }
}
