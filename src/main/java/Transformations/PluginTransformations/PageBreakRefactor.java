package Transformations.PluginTransformations;


import GraphData.DatabaseConnection.Neo4jReader;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class PageBreakRefactor {




    private String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    private LogicalGraph inputgraph;
    private final  String description = "Does a plain Pagebreak refactor on a DTA Database";
    private static final long serialVersionUID = 10L;


    public PageBreakRefactor(){}

    public PageBreakRefactor(Neo4jReader reader, String continuationPattern){
        //this.reader =  super.reader;
        //this.continuationPattern = continuationPattern;
        //this.writer = super.writer;
    }


    //@Override
    public LogicalGraph execute(LogicalGraph lg) {
        this.inputgraph = lg;
        refactor();
        LogicalGraph nextGraph = null;
       // if(continuationPattern.length()<2 || continuationPattern == null) {
         //   nextGraph = this.reader.readGraph(this.continuationPattern);

       //     try {
        //        GDLEncoder enc = new GDLEncoder(nextGraph.getGraphHead().collect(),
                      //  nextGraph.getVertices().collect(), nextGraph.getEdges().collect());
          //      stringRep = enc.getGDLString();
           // } catch (Exception e) {
              //  e.printStackTrace();
            //}
       //}
       // reader.close();
       // writer.close();
       // return nextGraph;
        return lg;
    }

    private void refactor(){
        LogicalGraph lg = extractPattern1();
        firstRefactor(lg);
       // secondRefactor();

    }

    private void secondRefactor(){

       LogicalGraph lg =  extractPattern2();
       LogicalGraph res  = connectPageNodes(lg,getPageIndices(lg));
      // this.writer.putGraph(lg,res);

    }


    private LogicalGraph extractPattern1(){


        String pattern =""+
                "MATCH " +
                "(n:XmlWord)-[e:NEXT]->(t1:XmlTag)-[e1:NEXT_SIBLING]->(t2:XmlTag)-[e2:NEXT_SIBLING]" +
                "->(t3:XmlTag)-[e3:NEXT_SIBLING]->(t4:XmlTag), "+
                "(t4)-[e4:NEXT_SIBLING]->(n2:XmlWord), "+
                "(t2)-[e7:NEXT]->(n4:XmlWord), "+
                "(n)-[e5:NEXT_WORD]->(n3:XmlWord), "+
                "(n3:XmlWord)-[e6:NEXT_WORD]->(n5:XmlWord), "+
                "(n3)-[:NEXT]->(t3) "+
                "WHERE t1._name='lb' AND t2._name='fw' AND t3._name='lb' "+
                " RETURN *";

        try {

           // LogicalGraph lg = this.reader.readGraph(pattern);
            return null;
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;


    }


    public LogicalGraph extractPattern2(){
        String pattern = "MATCH (n1:XmlWord)-[e:NEXT_WORD]->(m1:XmlWord)," +
                         " (t:XmlTag)-[c:NEXT_SIBLING]->(m:XmlWord) "+
                         " WHERE t._name='pb'"+
                          "RETURN DISTINCT * ";
        try {
            return null ;
            // reader.readGraph(pattern);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }



    private LogicalGraph firstRefactor(LogicalGraph lg){

        String extractionPattern =""+
                "MATCH (n:XmlWord)-[e:NEXT]->(t1:XmlTag)-[e1:NEXT_SIBLING]" +
                "->(t2:XmlTag)-[e2:NEXT_SIBLING]->(t3:XmlTag)-[e6:NEXT_SIBLING]->(t4:XmlTag) "+
                "(t2)-[e3:NEXT]->(n2:XmlWord)<-[e4:NEXT_WORD]-(n) "+
                "(n2)-[e5:NEXT_WORD]->(n3:XmlWord) "+
                "WHERE t1._name = \"lb\"  AND t2._name = \"fw\" AND t3._name = \"lb\"  AND t4._name = \"pb\"  ";

        String creationPattern = "(n)-[e]->(t1)-[eNew1:NEXT]->(n2)-[eNew2:NEXT]->(t3)-[e6]->(t4)" +
                "(n)-[eNew3:CATCH_WORDS]->(n2) "+
                "(n)-[eNew4:NEXT_WORD]->(n3)";


        try{
            GraphCollection gc = lg.query(extractionPattern,creationPattern);
            ReduceCombination rc = new ReduceCombination();
            LogicalGraph result2 = (LogicalGraph) rc.execute(gc);

            GraphCollection gc2 = lg.query(extractionPattern);
            ReduceCombination rc2 = new ReduceCombination();
            LogicalGraph result1 = (LogicalGraph) rc2.execute(gc2);

           // this.writer.putGraph(result1,result2);

        }catch(Exception e){

            e.printStackTrace();
        }
        return lg;

    }

    private LogicalGraph connectPageNodes(LogicalGraph logicalGraph, List<String[]> indices){

        for(String[] ids : indices){
            try {

                logicalGraph = logicalGraph.combine(createPageNodes(logicalGraph, ids[0], ids[1]));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return logicalGraph;

    }



   private LogicalGraph createPageNodes(LogicalGraph lg, String begin, String end){
       String extractionPattern = ("MATCH "+
               "(n:XmlWord),(m:XmlWord) WHERE n."+CYPHER_SOURCE_ID+" = \""+begin+
               "\" AND m."+CYPHER_SOURCE_ID+" = \""+end+"\"");

       String constructionPattern = "(page:page)," +
               "(n)<-[f1:FIRST_CHILD_OF]-(page)," +
               "(page)-[f2:LAST_CHILD_OF]->(m)";

       try {


           GraphCollection gc = lg.query(extractionPattern, constructionPattern);
           List<EPGMEdge> resultEdges = gc.getEdges().collect();
           List<EPGMVertex> resultVertices = gc.getVertices().collect();
           for(EPGMEdge e : resultEdges){
               for (EPGMVertex v :resultVertices){
                   if(e.getLabel().equals("FIRST_CHILD_OF")){
                       if(v.getLabel().equals("page")){
                           e.setSourceId(v.getId());
                       }
                   }
                   if(e.getLabel().equals("LAST_CHILD_OF")){
                       if(v.getLabel().equals("page")){
                           e.setSourceId(v.getId());
                       }
                   }
               }
           }

           LogicalGraphFactory graphFac = (LogicalGraphFactory) lg.getFactory();
           EPGMGraphHead head = lg.getGraphHead().collect().get(0);

           GradoopIdSet idSet = new GradoopIdSet();
           idSet.add(head.getId());


           LogicalGraph result = graphFac.fromCollections(head, resultVertices, resultEdges);



           return result;
       }catch(Exception e){
           e.printStackTrace();
       }

       return null;

   }

    private List<String[]> getPageIndices (LogicalGraph logicalGraph) {
        List<String[]> indices = new LinkedList<>();
        try {
            GraphCollection pageBegins = logicalGraph.query("MATCH (n:XmlTag)-[:NEXT_SIBLING]->(m:XmlWord)" +
                    "WHERE n._name= 'pb' ", "(m)");
            GraphCollection pageEnds = pageBegins;


            List<EPGMVertex> beginnerVertexList = pageBegins.getVertices().collect();
            List<EPGMVertex> enderVertexList = pageEnds.getVertices().collect();
            List<EPGMEdge> nextEdges = logicalGraph.getEdges().collect();

            HashMap<GradoopId, EPGMEdge> sourceEdgeMap = mapEdgeSources(nextEdges);
            HashMap<GradoopId, EPGMVertex> endVertexMap = mapVertices(enderVertexList);



            for (EPGMVertex beginner : beginnerVertexList) {

                boolean successorFound = false;


                EPGMEdge currentEdge = sourceEdgeMap.get(beginner.getId());

                while (!successorFound) {
                    if (currentEdge == null) {
                        break;
                    }
                    if (endVertexMap.get(currentEdge.getTargetId()) != null) {
                        EPGMVertex endVertex = endVertexMap.get(currentEdge.getTargetId());
                        String[] current = {beginner.getPropertyValue(CYPHER_SOURCE_ID).toString()
                                    , endVertex.getPropertyValue(CYPHER_SOURCE_ID).toString()};

                        indices.add(current);
                        successorFound = true;
                    } else {
                        currentEdge = sourceEdgeMap.get(currentEdge.getTargetId());
                    }
                }

            }
            return indices;


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    private static HashMap<GradoopId,EPGMVertex> mapVertices (List<EPGMVertex> beginners){

        HashMap<GradoopId,EPGMVertex> map = new HashMap<>();
        for (EPGMVertex vert : beginners){
            map.put(vert.getId(),vert);
        }
        return map;
    }

    private  static HashMap<GradoopId,EPGMEdge> mapEdgeSources (List<EPGMEdge> edges){

        HashMap<GradoopId,EPGMEdge> map = new HashMap<>();
        for (EPGMEdge edge : edges){
            map.put(edge.getSourceId(),edge);
        }
        return map;
    }


    public String getCYPHER_SOURCE_ID() {
        return CYPHER_SOURCE_ID;
    }

    public void setCYPHER_SOURCE_ID(String CYPHER_SOURCE_ID) {
        this.CYPHER_SOURCE_ID = CYPHER_SOURCE_ID;
    }

    public LogicalGraph getInputgraph() {
        return inputgraph;
    }


    public void setInputgraph(LogicalGraph inputgraph) {
        this.inputgraph = inputgraph;
    }

    //@Override
    public String getDescription() {
        return description;
    }
}


