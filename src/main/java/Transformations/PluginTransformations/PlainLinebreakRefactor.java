package Transformations.PluginTransformations;


import GraphData.DatabaseConnection.Neo4jReader;
import Transformations.PluginTransformation;
import org.apache.hadoop.util.hash.Hash;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;

import java.io.Serializable;
import java.util.*;


public class PlainLinebreakRefactor extends PluginTransformation implements Serializable{


    private String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    private final String description = "Does a plain Linebreak refactor on a DTA Database. " +
            "\n This Transformations runs autonomously, so you don't need\n "+
            "to pass a specific pattern. The continuation Pattern is directly called \n"+
            "on the associated Database and returns the extracted Graph.";
    private transient LogicalGraph inputgraph;
    private static final long serialVersionUID = 9L;
    private transient LogicalGraph resultGraph;


    public PlainLinebreakRefactor(){}

    public PlainLinebreakRefactor(String continuationPattern){

        this.reader = super.getReader();
        this.continuationPattern = super.getContinuationPattern();
        if (continuationPattern == null){
            this.continuationPattern = "MATCH (n) RETURN n LIMIT 1";
        }



    }

    @Override
    public LogicalGraph execute(LogicalGraph lg) {
        this.inputgraph = lg;
        this.resultGraph = lg.copy();

        refactor();

        resultGraph = queryGraph(resultGraph,continuationPattern);

        if(continuationPattern.length()<2 || continuationPattern == null) {
            LogicalGraph nextGraph = queryGraph(resultGraph,continuationPattern);
            try {
                GDLEncoder enc = new GDLEncoder(nextGraph.getGraphHead().collect(),
                        nextGraph.getVertices().collect(), nextGraph.getEdges().collect());
                stringRep = enc.getGDLString();
                super.setStringRep(stringRep);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }else{
            try {
                GDLEncoder enc = new GDLEncoder(resultGraph.getGraphHead().collect(),
                        resultGraph.getVertices().collect(), resultGraph.getEdges().collect());
                stringRep = enc.getGDLString();
                super.setStringRep(stringRep);
                super.setResultGraph(this.resultGraph);

                this.resultGraph.print();

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return resultGraph;

    }

    private void refactor(){

        LogicalGraph tmp = extractPattern();

        List<String[]> inds = getLineIndices(tmp);

        resultGraph = createLineNodes(inputgraph,inds);


        resultGraph = deleteLineBreakNodes(resultGraph);

        //resultGraph = mergeLinebreakNodes(resultGraph,inds);

    }



    private LogicalGraph extractPattern(){

        String pattern = "MATCH " +
                "  (n1:XmlWord)-[e:NEXT_WORD]->(m1:XmlWord)," +
                "  (t:XmlTag)-[c:NEXT]->(m:XmlWord),"+
                "  (n:XmlWord)-[f:NEXT]->(t:XmlTag) WHERE t._name='lb'";

        try {

            LogicalGraph result = queryGraph(inputgraph,pattern);
            return result;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;

    }


    private List<String[]> getLineIndices (LogicalGraph logicalGraph){
        List<String[]> indices = new LinkedList<>();
        try {
            GraphCollection lineBegins = logicalGraph.query("MATCH (n:XmlTag)-[:NEXT]->(m:XmlWord)" +
                    "WHERE n._name= 'lb' ","(m)");
            GraphCollection lineEnds = logicalGraph.query("MATCH (n:XmlWord)-[:NEXT]->(m:XmlTag)" +
                    "WHERE m._name= 'lb' ","(n)");
            List<EPGMVertex>  beginnerVertexList = lineBegins.getVertices().collect();
            List<EPGMVertex> enderVertexList = lineEnds.getVertices().collect();
            List<EPGMEdge> nextEdges = logicalGraph.getEdges().collect();
            HashMap<GradoopId,EPGMEdge> sourceEdgeMap = mapEdgeSources(nextEdges);
            HashMap<GradoopId,EPGMVertex> endVertexMap = mapVerticesByGradoopID(enderVertexList);

            for(EPGMVertex beginner : beginnerVertexList) {
                boolean successorFound = false;

                EPGMEdge currentEdge = sourceEdgeMap.get(beginner.getId());
                while (!successorFound) {
                    if(currentEdge == null) {break;}
                    if(endVertexMap.get(currentEdge.getTargetId())!=null){
                        EPGMVertex endVertex = endVertexMap.get(currentEdge.getTargetId());
                        String[] current = {beginner.getPropertyValue(CYPHER_SOURCE_ID).toString()
                                ,endVertex.getPropertyValue(CYPHER_SOURCE_ID).toString()};
                        indices.add(current);
                        successorFound = true;
                    }else{
                        currentEdge = sourceEdgeMap.get(currentEdge.getTargetId());
                    }
                }
            }
            return indices;

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;



    }


    private LogicalGraph createLineNodes(LogicalGraph logicalGraph, List<String[]> indices){

        for(String[] ids : indices){
            try {
                logicalGraph = logicalGraph.combine(connectNodesAsLine(logicalGraph, ids[0], ids[1]));


            }catch(Exception e){
                e.printStackTrace();
                continue;
            }
        }

        return logicalGraph;

    }

    private LogicalGraph connectNodesAsLine(LogicalGraph logicalGraph, String begin, String end){


        String extractionPattern = ("MATCH"+
                "(n:XmlWord),(m:XmlWord) WHERE n."+CYPHER_SOURCE_ID+" = \""+begin+
                "\" AND m."+CYPHER_SOURCE_ID+" = \""+end+"\"");

        String constructionPattern = "" +
                "(l:line)," +
                "(n)<-[f1:FIRST_CHILD_OF]-(l:line)," +
                "(l:line)-[f2:LAST_CHILD_OF]->(m)";

        try {


            GraphCollection gc = logicalGraph.query(extractionPattern, constructionPattern);
            List<EPGMEdge> resultEdges = gc.getEdges().collect();
            List<EPGMVertex> resultVertices = gc.getVertices().collect();
            for(EPGMEdge e : resultEdges){
                for (EPGMVertex v :resultVertices){
                    if(e.getLabel().equals("FIRST_CHILD_OF")){
                        if(v.getLabel().equals("line")){
                            e.setSourceId(v.getId());
                            v.setProperty("name","Line");
                        }
                    }
                    if(e.getLabel().equals("LAST_CHILD_OF")){
                        if(v.getLabel().equals("line")){
                            e.setSourceId(v.getId());
                            v.setProperty("name","Line");
                        }
                    }
                }
            }

            LogicalGraphFactory graphFac = (LogicalGraphFactory) logicalGraph.getFactory();
            LogicalGraph result = graphFac.fromCollections(resultVertices, resultEdges);
            resultGraph = result;



            return resultGraph;


        }catch(Exception e){
               e.printStackTrace();
        }

        return resultGraph;


    }


    private LogicalGraph deleteLineBreakNodes(LogicalGraph lg){

        String extractionPattern = ""+
                "MATCH (l1:line)-[e1:LAST_CHILD_OF]->(n:XmlWord),"+
                "(n)-[e2:NEXT]->(lb:XmlTag)-[e3:NEXT]->(m:XmlWord),"+
                "(l2:line)-[e4:FIRST_CHILD_OF]->(m)";

        GraphCollection gc1 = lg.query(extractionPattern);


        ReduceCombination rc = new ReduceCombination();

        LogicalGraph first = (LogicalGraph) rc.execute(gc1);

        try {
            List<EPGMVertex> vertices = first.getVerticesByLabel("XmlTag").collect();

            List<EPGMVertex> resultVertices = lg.getVertices().collect();
            List<EPGMEdge> resultEdges = lg.getEdges().collect();

            for(EPGMVertex v : vertices) {

                String id = v.getPropertyValue(CYPHER_SOURCE_ID).toString();

                for(int i = 0; i< resultVertices.size();i++){
                    if(resultVertices.get(i).hasProperty(CYPHER_SOURCE_ID)){


                        if(resultVertices.get(i).getPropertyValue(CYPHER_SOURCE_ID).getString().equals(id)){
                            resultVertices.remove(i);

                        }
                    }
                }



            }

            LogicalGraphFactory graphFac = (LogicalGraphFactory) lg.getFactory();
            LogicalGraph result = graphFac.fromCollections(resultVertices, resultEdges);

            this.resultGraph = result;
            return result;


        }catch(Exception e){
            e.printStackTrace();
        }

        return resultGraph;


    }



    private LogicalGraph mergeLinebreakNodes(LogicalGraph logicalGraph, List<String[]> inds){


        try {

            // LogicalGraph tmp = logicalGraph.copy();
            // List<EPGMVertex> verts = tmp.getVerticesByLabel("XmlWord").collect();
            // verts.removeIf(v->!(v.getPropertyValue("text").toString().endsWith("-")));


            List<EPGMEdge> edgeList = logicalGraph.getEdges().collect();
            HashMap<GradoopId,EPGMVertex> vertices = mapVerticesByGradoopID(logicalGraph.getVertices().collect());
            HashMap<GradoopId,EPGMEdge> edges = mapNextWordEdgeSources(edgeList);



            HashMap<GradoopId,EPGMVertex> vertices2 = new HashMap<>();


            for(Map.Entry entry : vertices.entrySet()) {

                vertices2.put((GradoopId)entry.getKey(),(EPGMVertex)entry.getValue());

            }



            for(Map.Entry entry : vertices.entrySet()){

                EPGMVertex vertex = (EPGMVertex) entry.getValue();

                if(vertex.hasProperty("text")){


                    String text = vertex.getPropertyValue("text").getString();
                    if(text.endsWith("-")){

                        EPGMEdge nextWord = edges.get(vertex.getId());
                        EPGMVertex succsessor = vertices.get(nextWord.getTargetId());

                        EPGMEdge nextLine = edges.get(succsessor.getId());
                        edges.remove(succsessor.getId());

                        nextLine.setSourceId(vertex.getId());
                        edges.put(vertex.getId(),nextLine);

                        String before = vertex.getPropertyValue("text").getString();
                        before = before.replaceFirst("-","");

                        String after = succsessor.getPropertyValue("text").getString();
                        String textNew = before + after;


                        vertex.setProperty("before",before);
                        vertex.setProperty("after",after);
                        vertex.setProperty("text",textNew);

                        vertices2.remove(vertex.getId());
                        vertices2.put(vertex.getId(),vertex);
                        vertices2.remove(succsessor.getId());

                        // Cypher ID'S WEG
                        edges.get(succsessor.getId()).removeProperty(CYPHER_SOURCE_ID);


                    }


                }

            }


            LogicalGraphFactory graphFac = (LogicalGraphFactory) logicalGraph.getFactory();

            ArrayList<EPGMVertex> finalVertices = new ArrayList<EPGMVertex>(vertices2.values());
            ArrayList<EPGMEdge> finalEdges = new ArrayList<EPGMEdge>(edges.values());

            for(EPGMEdge e:edgeList){
                if(!finalEdges.contains(e)){
                    finalEdges.add(e);
                }
            }

            finalEdges.forEach(e->System.out.println(e.toString()));


            this.resultGraph = graphFac.fromCollections(finalVertices,finalEdges);

            return resultGraph;

        }catch(Exception e){
            e.printStackTrace();
        }

        return resultGraph;

    }



    private static HashMap<GradoopId,EPGMVertex> mapVerticesByGradoopID (List<EPGMVertex> beginners){

        HashMap<GradoopId,EPGMVertex> map = new HashMap<>();
        for (EPGMVertex vert : beginners){
            map.put(vert.getId(),vert);
        }
        return map;
    }

    private  static HashMap<GradoopId,EPGMEdge> mapNextWordEdgeSources (List<EPGMEdge> edges){

        HashMap<GradoopId,EPGMEdge> map = new HashMap<>();
        for (EPGMEdge edge : edges){
            if(edge.getLabel().equals("NEXT_WORD")) {
                map.put(edge.getSourceId(), edge);
            }
        }
        return map;
    }


    private  static HashMap<GradoopId,EPGMEdge> mapEdgeSources (List<EPGMEdge> edges){

        HashMap<GradoopId,EPGMEdge> map = new HashMap<>();
        for (EPGMEdge edge : edges){
            map.put(edge.getSourceId(),edge);
        }
        return map;
    }

    private LogicalGraph queryGraph(LogicalGraph lg,String query){


        GraphCollection gc = lg.query(query);


        ReduceCombination rc = new ReduceCombination();

        return (LogicalGraph) rc.execute(gc);
    }

    public String getCYPHER_SOURCE_ID() {
        return CYPHER_SOURCE_ID;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getStringRep(){
        return this.stringRep;
    }

    public LogicalGraph getInputgraph() {
        return inputgraph;
    }
}
