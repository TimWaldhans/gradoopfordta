package Transformations.PluginTransformations;


import GraphData.DatabaseConnection.Neo4jReader;
import Transformations.PluginTransformation;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Property;
import org.gradoop.dataintegration.transformation.impl.PropertyTransformation;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;




public class ExamplePluginTransformation{



    private final String description = ""+
            "This example Plugin Transformation transforms\n"+
            "given \"Name\" attributes of a Node into \n"+
            "two Attributes \"First Name\" and \"Second name\" \n"+
            "if they are a double name split by \"-\" ";
    private LogicalGraph inputgraph;
    private static final long serialVersionUID = 99L;
    private LogicalGraph resultGraph;


    public ExamplePluginTransformation(){

    }


    public ExamplePluginTransformation(String continuationPattern){

        // Gruppierung und Aus Datum Alter errechnen




    }


    public  LogicalGraph execute(LogicalGraph lg) {
        this.inputgraph = lg;



        LogicalGraph tmpGraph = lg
                .subgraph(v -> true, e -> true)
                .transformVertices((currentVertex, transformedVertex) -> {
                    if(currentVertex.hasProperty("name")){
                        String name = currentVertex.getPropertyValue("name").getString();
                        if(name.contains("-")){
                           String[] nameSplit = name.split("-");
                           String firstName = nameSplit[0];
                           String secondName = nameSplit[1];
                           currentVertex.removeProperty("name");

                           currentVertex.setProperty("FirstName",firstName);
                            currentVertex.setProperty("SecondName",secondName);

                        }

                    }
                    return currentVertex;
                });

      //  GraphCollection gc = tmpGraph.query(continuationPattern);
        ReduceCombination rc = new ReduceCombination();
     //   tmpGraph = (LogicalGraph) rc.execute(gc);
      //  super.setResultGraph(resultGraph);




     //   super.setResultGraph(tmpGraph);
        this.resultGraph = tmpGraph;
        try {
            GDLEncoder enc = new GDLEncoder(resultGraph.getGraphHead().collect(),
                    resultGraph.getVertices().collect(), resultGraph.getEdges().collect());
            String str = enc.getGDLString();
     //       super.setStringRep(str);
     //       this.stringRep = str;
        }catch (Exception e){
            e.printStackTrace();
        }

        return tmpGraph;

    }

  //  @Override
  //  public String getStringRep(){
  //     return this.stringRep;
   // }


   // @Override
   // public String getDescription() {
   //     return description;
  //  }

    public LogicalGraph getInputgraph() {
        return inputgraph;
    }
}
