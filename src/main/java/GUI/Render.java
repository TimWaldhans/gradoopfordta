package GUI;


import GUI.Controller.Components.ListComponents;
import GUI.Controller.Components.PanelComponents;
import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Controller.Components.CenterPanel;

import javax.swing.*;
import java.awt.*;


/**
 * Main Class for GUI, controls Elements and their data representation
 */

public class Render extends JFrame {


    // associated Panels and Transformation Lists
    private static DefaultListModel<TransformationScheduleListElement> transformationList;
    private static JList<TransformationScheduleListElement> transactionScheduleContainerJList;
    static CenterPanel centerPanel;


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Render r = new Render();
                r.setVisible(true);
            }
        });
    }

    public Render() {

        transformationList = new DefaultListModel<>();
        setTitle("Gradoop for Neo4j");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
         setSize(screenSize.width, screenSize.height);

        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container rootPane = getContentPane();
        centerPanel = new CenterPanel();

        setJMenuBar(PanelComponents.renderMenuBar());
        BorderLayout mainLayout = new BorderLayout();


        rootPane.setLayout(mainLayout);
        rootPane.add(transactionScheduleListContainer(),BorderLayout.WEST);
        rootPane.add(PanelComponents.renderGraphResultRepresentationPanel(this.getContentPane()),BorderLayout.SOUTH);

        rootPane.add(centerPanel.getContainer(),BorderLayout.CENTER);


        setVisible(true);

    }

    private Container transactionScheduleListContainer(){

        Container pane = new Container();
        pane.setLayout(new BorderLayout());
        String text = "<html> Transformation Schedules </html>";
        JLabel transformationSchedules = new JLabel(text);
        pane.add(transformationSchedules,BorderLayout.NORTH);

        transactionScheduleContainerJList = ListComponents.
                renderScheduleList(transformationList,
                this,centerPanel);
        JScrollPane scrollableList = new JScrollPane(transactionScheduleContainerJList);
        pane.add(scrollableList,BorderLayout.CENTER);

        return pane;

    }

    public static CenterPanel getCenterPanel() {
        return centerPanel;
    }

    public static DefaultListModel<TransformationScheduleListElement> getTransformationList() {
        return transformationList;
    }

    public static void setTransformationList(DefaultListModel<TransformationScheduleListElement> transformationList) {
        Render.transformationList = transformationList;
    }
}
