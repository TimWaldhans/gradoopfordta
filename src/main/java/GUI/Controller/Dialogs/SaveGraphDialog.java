package GUI.Controller.Dialogs;

import GraphData.GraphFileHandler.GraphWriter;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * This class creates the dialog for Saving a Graph into the gdl format
 */

public class SaveGraphDialog extends JDialog {



    // Gui components
    private JLabel pathLB;
    private JTextField pathTF;
    private JLabel nameLB;
    private JTextField nameTF;
    private JButton btnSave;
    private JButton btnCancel;


    /**
     * Constructor - initiates a dialog for the user to specifiy where to save the graph
     * @param parent The parent Frame needed for dialogs
     * @param logicalGraph The Graph to save
     */

    public SaveGraphDialog (Frame parent, LogicalGraph logicalGraph){
        super(parent, "Please enter Path", true);


        getContentPane().removeAll();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        nameLB = new JLabel("Name:");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(nameLB, cs);

        nameTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(nameTF, cs);

        pathLB = new JLabel("Path:");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(pathLB, cs);

        pathTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(pathTF, cs);



        btnSave = new JButton("Save");

        btnSave.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                GraphWriter.writeGraphGDL(logicalGraph,pathTF.getText(),nameTF.getText());
                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnSave);
        bp.add(btnCancel);


        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());


    }






}
