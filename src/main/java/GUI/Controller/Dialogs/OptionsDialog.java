package GUI.Controller.Dialogs;

import GUI.Controller.Schedule.TransformationScheduleListElement;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Renders the Dialog for altering Schedule credentials
 */

public class OptionsDialog extends JDialog {




    // Textinput components for credentials
    private JTextField tfURI;
    private JTextField tfUsername;
    private JTextField tfName;
    private JTextField pfPassword;
    private JLabel lbName;
    private JLabel lbUsername;
    private JLabel lbPassword;
    private JLabel  lbURI;
    private JButton btnLogin;
    private JButton btnCancel;
    // Creates schedule element
    private TransformationScheduleListElement transformationScheduleListElement;


    /**
     * Renders the options dialog element
     * @param parent The parent frame needed for dialogs
     * @param ele The Transformation Schedule to add the credentials
     */


    public OptionsDialog (Frame parent,TransformationScheduleListElement ele){
        super(parent, "Please enter Database credentials", true);

        transformationScheduleListElement = ele;
        if(ele.isLocal()){
        renderLocal();
        }
        if(!ele.isLocal()){
        renderRemote();
        }

    }

    // Renders a input dialog for Remote databases
    private void renderRemote(){

        getContentPane().removeAll();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        lbName = new JLabel("Name:");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lbName, cs);

        tfName= new JTextField(20);
        tfName.setText(transformationScheduleListElement.getNameAsString());
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(tfName, cs);

        lbURI= new JLabel("Bolt protocol URI:");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lbURI, cs);

        tfURI = new JTextField(20);
        tfURI.setText(transformationScheduleListElement.getURI());
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(tfURI, cs);

        lbUsername = new JLabel("Username: ");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 1;
        panel.add(lbUsername, cs);

        tfUsername = new JTextField(20);
        tfUsername.setText(transformationScheduleListElement.getUsername());
        cs.gridx = 1;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(tfUsername, cs);

        lbPassword = new JLabel("Password: ");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 1;
        panel.add(lbPassword, cs);

        pfPassword = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 3;
        cs.gridwidth = 2;
        panel.add(pfPassword, cs);
        panel.setBorder(new LineBorder(Color.GRAY));



        btnLogin = new JButton("Set");

        btnLogin.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                JLabel name = new JLabel();
                name.setText(tfName.getText());

                transformationScheduleListElement.setName(name);
                transformationScheduleListElement.setURI(tfURI.getText());
                transformationScheduleListElement.setUsername(tfUsername.getText());
                transformationScheduleListElement.setPw(pfPassword.getText());

                transformationScheduleListElement.getTransformationSchedule().setURI(tfURI.getText());
                transformationScheduleListElement.getTransformationSchedule().setUser(tfUsername.getText());
                transformationScheduleListElement.getTransformationSchedule().setPw(pfPassword.getText());


                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnLogin);
        bp.add(btnCancel);


        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());

    }

    // Renders a input dialog for Local databases
    private void renderLocal(){

        getContentPane().removeAll();
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        lbName = new JLabel("Name:");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(lbName, cs);

        tfName= new JTextField(20);
        tfName.setText(transformationScheduleListElement.getNameAsString());
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(tfName, cs);

        lbURI= new JLabel("Path:");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(lbURI, cs);

        tfURI = new JTextField(20);
        tfName.setText(transformationScheduleListElement.getURI());
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(tfURI, cs);




        btnLogin = new JButton("Set");

        btnLogin.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                JLabel name = new JLabel();
                name.setText(tfName.getText());
                transformationScheduleListElement.setName(name);
                transformationScheduleListElement.setURI(tfURI.getText());

                transformationScheduleListElement.getTransformationSchedule().setPath(tfURI.getText());
                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {


                dispose();
            }
        });


        JPanel bp = new JPanel();
        bp.add(btnLogin);
        bp.add(btnCancel);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());

    }

    public TransformationScheduleListElement getElement(){
        return transformationScheduleListElement;
    }









}
