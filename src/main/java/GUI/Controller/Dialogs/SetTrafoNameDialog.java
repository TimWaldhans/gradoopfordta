package GUI.Controller.Dialogs;

import GUI.Controller.Schedule.TransformationElement;
import GraphData.Serialization.SerializationFileHandler;
import Transformations.CustomTransformations.BasicTransformation;
import Transformations.GradoopTransformations.AdvancedTransformation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SetTrafoNameDialog extends JDialog {



    // Text input elements for the path
    private JLabel nameLB;
    private JTextField nameTF;
    // Buttons for panel
    private JButton btnSave;
    private JButton btnCancel;

    /**
     * Generates the schedule for importing the schedule
     * @param parent The parent Frame for the dialog
     * @see SerializationFileHandler
     */

    public SetTrafoNameDialog(Frame parent, TransformationElement element, JList listView){
        super(parent, "Please enter Name", true);


        getContentPane().removeAll();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        nameLB = new JLabel("Name:");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(nameLB, cs);

        nameTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(nameTF, cs);


        btnSave = new JButton("Rename");

        btnSave.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {


                element.setName(nameTF.getText());
                if(element.getTransformation() instanceof AdvancedTransformation
                        || element.getTransformation() instanceof BasicTransformation){

                    listView.revalidate();
                    listView.updateUI();
                    element.render();

                }

                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnSave);
        bp.add(btnCancel);


        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());


    }
}
