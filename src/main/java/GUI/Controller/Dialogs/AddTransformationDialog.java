package GUI.Controller.Dialogs;

import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Controller.Schedule.TransformationElement;

import Transformations.CustomTransformations.BasicTransformation;
import Transformations.GradoopTransformations.AdvancedTransformation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * This class handles the Dialog for adding a new
 * Transformation the the schedule
 */

public class AddTransformationDialog extends JDialog {


    /**
     * Creates a dialog for adding a transformation
     * @param parent The parentframe for the dialog
     * @param transformationList The adapter List for transformation Lists
     * @param scheduleListElement The associated schedule
     * @see TransformationElement
     * @see TransformationScheduleListElement
     */


    public AddTransformationDialog(Frame parent, DefaultListModel<TransformationElement> transformationList,
                                   TransformationScheduleListElement scheduleListElement) {
        super(parent, "Please pick Transformation Type", true);



        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;


        JLabel label = new JLabel("Name: ");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(label, cs);

        JTextField tfName = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(tfName, cs);

        JButton btnGradoopCustom = new JButton("Basic Transformation");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(btnGradoopCustom, cs);
        btnGradoopCustom.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isLeftMouseButton(me)) {
                    transformationList.addElement(
                            new TransformationElement(new BasicTransformation(),
                                    tfName.getText(),scheduleListElement));

                    dispose();

                }
            }
        });

        JButton btnGradoop = new JButton("Advanced Transformation");
        cs.gridx = 0;
        cs.gridy = 2;
        cs.gridwidth = 2;
        panel.add(btnGradoop, cs);
        btnGradoop.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isLeftMouseButton(me)) {
                    transformationList.addElement(
                            new TransformationElement(new AdvancedTransformation(),
                                    tfName.getText(),scheduleListElement));

                    dispose();

                }
            }
        });



        JButton btnPlugin = new JButton(" Plugin Transformation");
        cs.gridx = 0;
        cs.gridy = 3;
        cs.gridwidth = 2;
        panel.add(btnPlugin, cs);
        btnPlugin.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isLeftMouseButton(me)) {

                    AddPluginTrafoDialog diag = new AddPluginTrafoDialog
                            (parent,transformationList,scheduleListElement);

                    dispose();

                }
            }
        });

        getContentPane().add(panel, BorderLayout.CENTER);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }




}
