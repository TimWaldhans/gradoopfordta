package GUI.Controller.Dialogs;

import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Controller.Schedule.TransformationElement;
import Transformations.PluginTransformation;
import org.reflections.Reflections;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

public class AddPluginTrafoDialog extends JDialog {


    public AddPluginTrafoDialog(Frame parent, DefaultListModel<TransformationElement> transformationList,
                                   TransformationScheduleListElement scheduleListElement) {

            Reflections reflections = new Reflections("Transformations.PluginTransformations");
            Set<Class<? extends PluginTransformation>> classes =  reflections.
                    getSubTypesOf(PluginTransformation.class);


           Class[] classArr= new Class[classes.size()];
            int i = 0;
            for(Class c : classes){
                classArr[i] =c ;
                i++;
            }


                Class input = (Class) JOptionPane.showInputDialog(null, "Pick Plugin class",
                "Plugin Classes Loaded", JOptionPane.QUESTION_MESSAGE, null,
                classArr,
                classArr[0]);


                 try {
                     PluginTransformation p = (PluginTransformation) input.getConstructor().newInstance();
                     String name = p.getClass().getSimpleName();

                     transformationList.addElement(new TransformationElement(p,name,scheduleListElement));
                 }catch(Exception e){
                     e.printStackTrace();
                 }


    }

    }


