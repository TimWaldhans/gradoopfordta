package GUI.Controller.Dialogs;

import GUI.Controller.Schedule.TransformationScheduleListElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Class for generating the Dialog for importing a single Graph
 */

public class LoadGraphDialog extends JDialog {


    // Path input text components
    private JLabel pathLB;
    private JTextField pathTF;
    // Buttons for handling
    private JButton btnSave;
    private JButton btnCancel;

    /**
     * Renders the dialog for loading a single Graph
     * @param parent The parent frame for the dialog
     * @param element the Transformation schedule to add the Graph to
     * @see TransformationScheduleListElement
     */


    public LoadGraphDialog(Frame parent, TransformationScheduleListElement element) {
        super(parent, "Please enter Path", true);


        getContentPane().removeAll();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;


        pathLB = new JLabel("Directory:");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(pathLB, cs);

        pathTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(pathTF, cs);


        btnSave = new JButton("Import");

        btnSave.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                element.getTransformationSchedule().importGraph(pathTF.getText());
                element.render();
                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnSave);
        bp.add(btnCancel);


        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());


    }


}
