package GUI.Controller.Dialogs;

import GUI.Controller.Schedule.TransformationScheduleListElement;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

/**
 * Renders the Dialog for adding a new transformation schedule
 */


public class ScheduleDialog extends JDialog {



        // Textinput components for credentials
        private JTextField tfURI;
        private JTextField tfUsername;
        private JTextField tfName;
        private JTextField pfPassword;
        private JLabel lbName;
        private JLabel lbUsername;
        private JLabel lbPassword;
        private JLabel  lbURI;
        private JButton addButton;
        private JButton btnCancel;
        // Underlying Transformation Objects
        private TransformationScheduleListElement schedule;
        private DefaultListModel<TransformationScheduleListElement> transactionList;


    /**
     * Creates a dialog for adding a new Transformation schedule with its credentials
     * @param parent the parentframe for the dialog
     * @param transactionList The data adapter for the transformation schedule
     * @see TransformationScheduleListElement
     */

    public ScheduleDialog(Frame parent,DefaultListModel<TransformationScheduleListElement> transactionList) {
            super(parent, "Please enter Database credentials", true);

            this.transactionList = transactionList;


            JPanel panel = new JPanel(new GridBagLayout());
            JButton btnRemote = new JButton("Add Remote Credentials");
            JButton btnLocal = new JButton("Add Local Credentials");
            JButton btnCancel = new JButton("Cancel");

            btnRemote.setVisible(true);
            btnLocal.setVisible(true);
            btnCancel.setVisible(true);

            btnRemote.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {

                    renderRemote();

                }
            });

            btnLocal.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {

                    renderLocal();

                }
            });


            btnCancel.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            JPanel bp = new JPanel();

            bp.add(btnRemote);
            bp.add(btnLocal);
            bp.add(btnCancel);
            getContentPane().add(panel, BorderLayout.NORTH);
            getContentPane().add(bp, BorderLayout.PAGE_END);

            pack();
            setResizable(false);
            setLocationRelativeTo(parent);
        }


        //renders this dialog for Remote databases

        private void renderRemote(){

            getContentPane().removeAll();

            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints cs = new GridBagConstraints();
            cs.fill = GridBagConstraints.HORIZONTAL;

            lbName = new JLabel("Name:");
            cs.gridx = 0;
            cs.gridy = 0;
            cs.gridwidth = 1;
            panel.add(lbName, cs);

            tfName= new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 0;
            cs.gridwidth = 1;
            panel.add(tfName, cs);

            lbURI= new JLabel("Bolt protocol URI:");
            cs.gridx = 0;
            cs.gridy = 1;
            cs.gridwidth = 1;
            panel.add(lbURI, cs);

            tfURI = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 1;
            cs.gridwidth = 1;
            panel.add(tfURI, cs);

            lbUsername = new JLabel("Username: ");
            cs.gridx = 0;
            cs.gridy = 2;
            cs.gridwidth = 1;
            panel.add(lbUsername, cs);

            tfUsername = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 2;
            cs.gridwidth = 2;
            panel.add(tfUsername, cs);

            lbPassword = new JLabel("Password: ");
            cs.gridx = 0;
            cs.gridy = 3;
            cs.gridwidth = 1;
            panel.add(lbPassword, cs);

            pfPassword = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 3;
            cs.gridwidth = 2;
            panel.add(pfPassword, cs);
            panel.setBorder(new LineBorder(Color.GRAY));

            lbName.setVisible(true);
            tfName.setVisible(true);
            lbURI.setVisible(true);
            tfName.setVisible(true);
            lbUsername.setVisible(true);
            tfUsername.setVisible(true);
            lbPassword.setVisible(true);
            pfPassword.setVisible(true);

            panel.setVisible(true);


            addButton = new JButton("Add");

            addButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    schedule = new TransformationScheduleListElement(tfName.getText(),tfURI.getText(),tfUsername.getText(),
                            pfPassword.getText(), transactionList.size()+1);

                    transactionList.addElement(schedule);

                    dispose();




                }
            });
            btnCancel = new JButton("Cancel");
            btnCancel.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            JPanel bp = new JPanel();
            bp.add(addButton);
            bp.add(btnCancel);


            getContentPane().add(panel, BorderLayout.CENTER);
            getContentPane().add(bp, BorderLayout.PAGE_END);

            pack();
            setResizable(false);
            setLocationRelativeTo(super.getParent());

        }

        // renders this dialog for local databases
        private void renderLocal(){

            getContentPane().removeAll();
            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints cs = new GridBagConstraints();
            cs.fill = GridBagConstraints.HORIZONTAL;

            lbName = new JLabel("Name:");
            cs.gridx = 0;
            cs.gridy = 0;
            cs.gridwidth = 1;
            panel.add(lbName, cs);

            tfName= new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 0;
            cs.gridwidth = 1;
            panel.add(tfName, cs);

            lbURI= new JLabel("Path:");
            cs.gridx = 0;
            cs.gridy = 1;
            cs.gridwidth = 1;
            panel.add(lbURI, cs);

            tfURI = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 1;
            cs.gridwidth = 1;
            panel.add(tfURI, cs);




            addButton = new JButton("Add");

            addButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    schedule = new TransformationScheduleListElement(
                            tfName.getText(),tfURI.getText(),transactionList.size()+1);
                    transactionList.addElement(schedule);

                    dispose();

                }
            });
            btnCancel = new JButton("Cancel");
            btnCancel.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {


                    dispose();
                }
            });


            JPanel bp = new JPanel();
            bp.add(addButton);
            bp.add(btnCancel);

            getContentPane().add(panel, BorderLayout.CENTER);
            getContentPane().add(bp, BorderLayout.PAGE_END);

            pack();
            setResizable(false);
            setLocationRelativeTo(super.getParent());

        }





        public TransformationScheduleListElement getSchedule() {
        return schedule;
    }
}
