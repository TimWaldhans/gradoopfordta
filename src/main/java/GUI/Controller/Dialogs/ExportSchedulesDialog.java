package GUI.Controller.Dialogs;

import GraphData.Serialization.SerializationFileHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class which generates a dialog for exporting a transaction Schedule
 */

public class ExportSchedulesDialog extends JDialog {


    // Path text input
    private JLabel pathLB;
    private JTextField pathTF;
    // Name text input
    private JLabel nameLB;
    private JTextField nameTF;
    // Relevant buttons
    private JButton btnSave;
    private JButton btnCancel;

    /**
     * Generates a schedule for  exporting Transformation schedules
     * @param parent The parentframe for dialoges
     * @see SerializationFileHandler
     */

    public ExportSchedulesDialog(Frame parent){
        super(parent, "Please enter Path", true);


        getContentPane().removeAll();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        nameLB = new JLabel("Name:");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(nameLB, cs);

        nameTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(nameTF, cs);

        pathLB = new JLabel("Path:");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(pathLB, cs);

        pathTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(pathTF, cs);



        btnSave = new JButton("Export");

        btnSave.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                SerializationFileHandler.exportSchedules(pathTF.getText(),nameTF.getText());

                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnSave);
        bp.add(btnCancel);


        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());


    }






}
