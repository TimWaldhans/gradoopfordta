package GUI.Controller.Dialogs;

import GraphData.Serialization.SerializationFileHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class which generates a dialog for exporting a transaction Schedule
 */

public class ImportSchedulesDialog extends JDialog {



    // Text input elements for the path
    private JLabel pathLB;
    private JTextField pathTF;
    // Buttons for panel
    private JButton btnSave;
    private JButton btnCancel;

    /**
     * Generates the schedule for importing the schedule
     * @param parent The parent Frame for the dialog
     * @see SerializationFileHandler
     */

    public ImportSchedulesDialog(Frame parent){
        super(parent, "Please enter Path", true);


        getContentPane().removeAll();

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        pathLB = new JLabel("Path:");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(pathLB, cs);

        pathTF = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(pathTF, cs);




        btnSave = new JButton("Import");

        btnSave.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {


                SerializationFileHandler.ImportSchedules(pathTF.getText());

                dispose();

            }
        });
        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnSave);
        bp.add(btnCancel);


        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(super.getParent());


    }
}
