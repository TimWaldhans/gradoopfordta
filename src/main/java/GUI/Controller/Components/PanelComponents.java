package GUI.Controller.Components;

import GUI.Controller.Dialogs.ExportSchedulesDialog;
import GUI.Controller.Dialogs.ImportSchedulesDialog;
import GUI.Controller.Dialogs.ScheduleDialog;
import GUI.Render;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URI;

/**
 * This class handles some miscellaneous Panel components
 */

public class PanelComponents {


    // Infopanel components
    static JTextArea infoText;
    static JScrollPane infoPanel;

    // Writes a String to the console
    public static void callInformation(String info){
        if(infoPanel!=null && infoText!=null) {
            infoText.append("\n" + info);
            infoPanel.updateUI();
        }
    }

    // clears the console
    public static void clearInformatio(){
        infoText.setText("");
        infoPanel.updateUI();
    }

    /**
     * Renders a container for Graph representation strings
     * @param parent The Parent container
     * @return The created representationpanel
     */
    public static Container renderGraphResultRepresentationPanel(Container parent){
        Container pane = new Container();
        GridBagLayout layout = new GridBagLayout();
        layout.maximumLayoutSize(parent);;
        pane.setLayout(layout);
        infoText = new JTextArea();
        infoPanel= new JScrollPane(infoText);
        infoText.setEditable(false);
        infoPanel.setPreferredSize(new Dimension(1280,120));
        infoPanel.setMaximumSize(new Dimension(parent.getWidth(),120));

        pane.add(infoPanel);

        return pane;
    }



    public static JMenuBar renderMenuBar(){
        JMenuBar menuBar = new JMenuBar();
        fileMenuRender(menuBar);
        infoMenuRender(menuBar);




        return menuBar;


    }


    private static void infoMenuRender(JMenuBar menuBar){
        JMenu menu = new JMenu("Info");
        JMenuItem menuItem;
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription(
                "Info menu");
        menuBar.add(menu);

        menuItem = new JMenuItem("Gradoop on GitHub",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                        Desktop.getDesktop().browse(new URI("https://github.com/dbs-leipzig/gradoop"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Neo4j Cypher",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                try {
                    if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                        Desktop.getDesktop().browse(new URI("https://neo4j.com/developer/cypher-query-language/"));
                    }
                }catch(Exception e){

                }

            }

        });
        menu.add(menuItem);

        menuItem = new JMenuItem("This project's Repository",
                new ImageIcon("images/middle.gif"));
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                        Desktop.getDesktop().browse(new URI("https://bitbucket.org/TimWaldhans/gradoop4j/src/master/"));
                    }
                }catch(Exception e){

                }
            }

        });
        menu.add(menuItem);

    }



    private static void fileMenuRender(JMenuBar menuBar){


        JMenu menu = new JMenu("File");
        JMenuItem menuItem;
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription(
                "File menu");
        menuBar.add(menu);

        menuItem = new JMenuItem("New Schedule",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ScheduleDialog diag = new ScheduleDialog(Render.getFrames()[0],Render.getTransformationList());

                diag.pack();
                diag.setVisible(true);
            }

        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Import Schedule",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImportSchedulesDialog diag = new ImportSchedulesDialog(Render.getFrames()[0]);

                diag.setVisible(true);
            }

        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Export Schedule",
                new ImageIcon("images/middle.gif"));
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ExportSchedulesDialog diag = new ExportSchedulesDialog(Render.getFrames()[0]);

                diag.setVisible(true);
            }

        });
        menu.add(menuItem);

    }










}
