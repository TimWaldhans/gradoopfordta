package GUI.Controller.Components;

import GUI.Controller.Dialogs.OptionsDialog;
import GUI.Controller.Schedule.TransformationElement;
import GUI.Controller.Schedule.TransformationScheduleListElement;
import Transformations.CustomTransformations.BasicTransformation;
import Transformations.GradoopTransformations.AdvancedTransformation;
import Transformations.PluginTransformation;
import Transformations.Transformation;
import Transformations.TransformationAlgorithms.TransformationSchedule;
import org.apache.commons.lang.SerializationUtils;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Class for handling List components of the GUI
 */

public class ListComponents {



    private static JList transformationSchedules;


    /**
     * Renders the side Panel of Lists of transformation schedules
     * @param scheduleListElementDefaultListModel The Data adapter for the List containing the schedules
     * @param frame The mainframe to add the list to
     * @param centerPanel The center work panel
     * @return The rendered List
     */



    public static JList renderScheduleList(final DefaultListModel<TransformationScheduleListElement> scheduleListElementDefaultListModel, JFrame frame, final CenterPanel centerPanel) {


        transformationSchedules = new JList(scheduleListElementDefaultListModel);


        // Onchange Listener for updating the List
        transformationSchedules.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if(centerPanel == null){
                    centerPanel.removeAll();

                }else {
                    int index = transformationSchedules.getFirstVisibleIndex();
                    if(index != -1) {
                        TransformationScheduleListElement element =
                                (TransformationScheduleListElement) scheduleListElementDefaultListModel.get(index);
                        element.render();
                        centerPanel.getContainer().repaint();
                    }
                }
            }

        });


        // subMenu for Deleting a transformations


        final JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem delete = new JMenuItem("Delete");
        popupMenu.add(delete);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformationSchedules.getSelectedIndex();
                TransformationScheduleListElement ele = (TransformationScheduleListElement)
                        scheduleListElementDefaultListModel.get(selectedIndex);
                ele.purge();
                if(scheduleListElementDefaultListModel.size() == 1){
                    scheduleListElementDefaultListModel.removeAllElements();
                    if(centerPanel.getTransformationList() == null) {
                        centerPanel.getTransformationListContainer().remove(centerPanel.getTransformationPanel());
                        centerPanel.getTransformationListContainer().removeAll();
                        centerPanel.setCenterPanel(new Container(),new Container());
                        centerPanel.getTransformationListContainer().revalidate();

                    }
                    transformationSchedules.setModel(scheduleListElementDefaultListModel);
                    transformationSchedules.updateUI();
                }else {
                    scheduleListElementDefaultListModel.remove(transformationSchedules.getSelectedIndex());
                }
                transformationSchedules.updateUI();
            }
        });

        // Submenu for editing a Transformations credentials
        popupMenu.add(new JPopupMenu.Separator());
        JMenuItem editCredentials = new JMenuItem("Edit Credentials");
        editCredentials.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformationSchedules.getSelectedIndex();
                TransformationScheduleListElement ele =
                        (TransformationScheduleListElement)scheduleListElementDefaultListModel.get(selectedIndex);
                OptionsDialog diag = new OptionsDialog(frame,ele);
                scheduleListElementDefaultListModel.set(selectedIndex,diag.getElement());

                diag.setVisible(true);
                ele.render();
                transformationSchedules.updateUI();

            }
        });


        popupMenu.add(editCredentials);

        popupMenu.add(new JPopupMenu.Separator());
        JMenuItem copy = new JMenuItem("Copy");
        copy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformationSchedules.getSelectedIndex();
                TransformationScheduleListElement ele =
                        (TransformationScheduleListElement)scheduleListElementDefaultListModel.get(selectedIndex);

                TransformationScheduleListElement newElement;
         

                     newElement = (TransformationScheduleListElement) SerializationUtils.clone(ele);


                    scheduleListElementDefaultListModel.addElement(newElement);


                transformationSchedules.setModel(scheduleListElementDefaultListModel);
                transformationSchedules.updateUI();

            }
        });
        popupMenu.add(copy);

        transformationSchedules.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)
                        && !transformationSchedules.isSelectionEmpty()
                        && transformationSchedules.locationToIndex(me.getPoint())
                        == transformationSchedules.getSelectedIndex()) {
                    popupMenu.show(transformationSchedules, me.getX(), me.getY());
                }
                if (SwingUtilities.isLeftMouseButton(me)) {

                    if(scheduleListElementDefaultListModel.isEmpty()){
                        return;
                    }
                    TransformationScheduleListElement element =
                            (TransformationScheduleListElement)transformationSchedules.getSelectedValue();
                    element.render();

                }
            }
        });
        return transformationSchedules;

    }

    public static JList colorRender(JList list){

        list.setCellRenderer(new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                                                          boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                TransformationElement  val = (TransformationElement) value;
                Transformation trafo = val.getTransformation();
                if (trafo instanceof BasicTransformation) {
                    Color col = new Color (0,210,28,150);
                    setBackground(col);
                    if (isSelected) {
                        setBackground(Color.lightGray);
                    }
                }
                if (trafo instanceof AdvancedTransformation) {
                    Color col = new Color (0, 18, 163, 140);
                    setBackground(col);
                    if (isSelected) {
                        setBackground(Color.lightGray);
                    }
                } if (trafo instanceof PluginTransformation) {
                    Color col = new Color (210, 0, 151, 148);
                    setBackground(col);
                    if (isSelected) {
                        setBackground(Color.lightGray);
                    }
                }
                return c;
            }

        });

        return list;

    }












    public static JList getTransformationSchedules() {
        return transformationSchedules;
    }
}
