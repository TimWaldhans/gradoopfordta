package GUI.Controller.Components;

import GUI.Controller.Dialogs.LoadGraphDialog;
import GUI.Controller.Dialogs.SaveGraphDialog;
import GUI.Controller.Dialogs.ScheduleDialog;
import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Controller.Dialogs.AddTransformationDialog;
import GUI.Controller.Schedule.TransformationElement;
import Transformations.TransformationAlgorithms.TransformationSchedule;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * This class handles and generates Buttons and their functionality for
 * the GUI
 */


public class ButtonComponents {


    // Button for creating a new Transformation Schedule
    private static JButton addTransFormationScheduleAddButton;
    // Button for adding a new Transformation to a schedule
    private static JButton addTransformation;
    // Button for executing the Transformations in a given schedule
    private static JButton runSchedule;
    // Button for clearing the Infopanel
    private static JButton cleanButton;
    // Button for loading the result of a Cypherquery into this environment
    private static JButton loadGraph;
    // Button for commiting the result of all transformations to a database
    private static JButton commitButton;
    // Button for saving a Graph locally
    private static JButton saveGraph;
    // Button for running all transformationSchedules paralellized
    private static JButton parralelRun;


    /**
     * Creates teh button for adding a Graph transformation to a schedule
     * @param frame The parentframe to add the button to
     * @param transformationList The List of transformations associated with a transformation schedule
     * @param scheduleListElement The transformation
     * @param transformations The GUI List link to the transformations
     * @return The assigned Button
     * @see JFrame
     * @see TransformationElement
     * @see TransformationScheduleListElement
     * @see JList
     */


    public static JButton loadAddTransformationButton(JFrame frame, DefaultListModel<TransformationElement> transformationList,
                                              TransformationScheduleListElement scheduleListElement,
                                              JList transformations){


        addTransformation = new JButton("Add Transformation");
        addTransformation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AddTransformationDialog dialog = new AddTransformationDialog(frame,transformationList,scheduleListElement);
                scheduleListElement.setTransformationList(transformationList);
                dialog.setVisible(true);
                transformations.updateUI();
            }

        });

        return addTransformation;

    }


    /**
     * Creates the button for running the transformations of a schedule on a defined
     * schedule
     * @param transformationList The List for transformation data
     * @param schedule The schedule associated with this transformations
     * @return The assigned Button
     * @see TransformationElement
     * @see TransformationSchedule
     */


    public static JButton loadRunScheduleButton(DefaultListModel<TransformationElement> transformationList,
                                                TransformationSchedule schedule){
        runSchedule = new JButton("Run Transformations");
        runSchedule.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {

                    schedule.resetGrafos();
                    for(int i = 0; i< transformationList.size(); i++){

                        // adds passed transformation to schedule
                        schedule.addGrafo(transformationList.get(i));
                    }

                    schedule.run();


                }catch(NullPointerException np){
                    PanelComponents.callInformation("Keine Transformationen definiert!:" + np.getMessage());
                }catch(Exception e){
                    e.printStackTrace();
                    PanelComponents.callInformation("Keine Transformationen definiert!:" + e.getMessage());
                }
            }
        });
        return runSchedule;
    }

    /**
     * Button that clears the Infopanel of the GUI
     * @return The assigned Button
     */
    public static JButton loadCleanButton(){
        cleanButton = new JButton("Clear Infopanel");
        cleanButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent actionEvent) {
                PanelComponents.clearInformatio();
            }
        });
        return cleanButton;
    }


    /**
     * Creates the Button for loading a Graph from Neo4j cypher into this environment
     * @param scheduleListElement The associated Transformation Schedule GUI controller
     * @param queryText The query text for the Neo4j database
     * @param schedule The underlying transformation schedule
     * @param graphRepresentation The textreperesentation Panel for updating its value
     * @param graphRepPanel The Scrolpanel for the graph representation
     * @return The assigned Button
     * @see TransformationScheduleListElement
     * @see TransformationSchedule
     */

    public static JButton  loadGraphButton(TransformationScheduleListElement scheduleListElement,
                                 TextArea queryText, TransformationSchedule schedule,
                                 JTextArea graphRepresentation,JScrollPane graphRepPanel){

        loadGraph = new JButton("Load Graph From Neo4j");
        loadGraph.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent actionEvent) {

                PanelComponents.callInformation("Loading Graph...");

                if(scheduleListElement.isLocal()){
                    schedule.setCypherBaseQuery(queryText.getText());
                    if(queryText.getText().length()<=1){
                        PanelComponents.callInformation("Empty query! Make sure to define an initial Query Pattern for Cypher");
                    }
                    try {
                        Thread.sleep(1000);
                        schedule.loadGraphLocally();
                        PanelComponents.callInformation("Graph loaded!");
                        LogicalGraph lg = schedule.getBaseGraph();
                        GDLEncoder enc = new GDLEncoder(lg.getGraphHead().collect(),
                                lg.getVertices().collect(), lg.getEdges().collect());

                        graphRepresentation.setText("");
                        String gdl = enc.getGDLString();
                        scheduleListElement.render();
                        graphRepresentation.setText(gdl);
                        graphRepPanel.updateUI();
                        return;

                    }catch(Exception e){
                        PanelComponents.callInformation(e.getMessage());
                    }

                }if(!scheduleListElement.isLocal()){
                    schedule.setCypherBaseQuery(queryText.getText());
                    if(queryText.getText().length()<=1){
                        PanelComponents.callInformation("Empty query! Make sure to define an initial Query Pattern for Cypher");
                    }
                    try {
                        schedule.loadGraphRemotely();
                        PanelComponents.callInformation("Graph loaded!");
                        LogicalGraph lg = schedule.getBaseGraph();
                        GDLEncoder enc = new GDLEncoder(lg.getGraphHead().collect(),
                                lg.getVertices().collect(), lg.getEdges().collect());

                        graphRepresentation.setText("");
                        String gdl = enc.getGDLString();
                        graphRepresentation.setText(gdl);
                        graphRepPanel.updateUI();

                        scheduleListElement.render();
                        return;
                    }catch(Exception e){
                        PanelComponents.callInformation(e.getMessage());
                    }

                }

            }
        });


        return loadGraph;
    }

    /**
     * Creates the button for commiting the resulting graph to the database
     * @param schedule The schedule associated with this transformations
     * @return The assigned Button
     * @see TransformationSchedule
     */

    public static JButton loadCommitButton(TransformationSchedule schedule){
        commitButton = new JButton("Commit to Database");
        commitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent actionEvent) {
                try {
                    if (schedule.getfinalGraph() == null || schedule.getfinalGraph().isEmpty().collect().get(0)) {
                        PanelComponents.callInformation("No resulting Graph detected. Make sure to run the" +
                                " transformation(s) first.");
                    }else{
                        PanelComponents.callInformation("Commiting to Database");
                        schedule.commit();

                    }
                }catch (Exception e){
                    PanelComponents.callInformation(e.getMessage());
                }
            }
        });
        return commitButton;


    }


    /**
     * Button for saving a graph in the GDL format of Gradoop
     * @param frame The parentframe of the GUI
     * @param lg the Logical Graph to save
     * @see GDLEncoder
     * @see LogicalGraph
     */

    public static JButton saveGraphButton(JFrame frame,LogicalGraph lg){
        saveGraph = new JButton("Save Graph");
        saveGraph.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent actionEvent) {
                try{
                    SaveGraphDialog dialog = new SaveGraphDialog(frame,lg);

                    dialog.setVisible(true);
                }catch(Exception e){
                    PanelComponents.callInformation(e.getMessage());
                }


            }
        });
        return saveGraph;


    }


    /**
     * Button for loading a graph in the GDL format of Gradoop into this environment
     * @param frame The parentframe of the GUI
     * @param element The target transformation schedule
     * @see TransformationScheduleListElement
     */

    public static JButton importGraphButton(JFrame frame,TransformationScheduleListElement element){
        saveGraph = new JButton("Import Graph");
        saveGraph.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent actionEvent) {
                try{
                    LoadGraphDialog dialog = new LoadGraphDialog(frame,element);

                    dialog.setVisible(true);
                }catch(Exception e){
                    PanelComponents.callInformation(e.getMessage());
                }


            }
        });
        return saveGraph;


    }



    public static JButton getAddTransFormationScheduleAddButton() {
        return addTransFormationScheduleAddButton;
    }


}

