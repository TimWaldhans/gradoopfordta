package GUI.Controller.Components;

import GUI.Controller.Schedule.TransformationScheduleListElement;
import Transformations.Transformation;
import Transformations.TransformationAlgorithms.TransformationSchedule;

import javax.swing.*;
import java.awt.*;


/**
 * This class handles and generates the main work panel
 */
public class CenterPanel extends JFrame {


    // GUI containers
    private Container transformationListContainer;
    private Container centerPanel;
    private Container transformationPanel;
    private Container buttonpanel;
    //The schedules associated with the current Panel and its GUI controller
    private TransformationScheduleListElement scheduleListElement;
    private TransformationSchedule schedule ;
    // The List of transformations associated with this schedule
    private DefaultListModel<Transformation> transformationList;


    /**
     * Initiates the current main panel
     */

    public CenterPanel(){


            this.transformationListContainer = new Container();

            BorderLayout mainLayout = new BorderLayout();

            transformationListContainer.setLayout(mainLayout);
            transformationPanel = new Container();
            centerPanel = new Container();
            buttonpanel = new Container();
            transformationListContainer.add(centerPanel,BorderLayout.CENTER);
            transformationListContainer.add(transformationPanel,BorderLayout.EAST);

    }



    public void setCenterPanel(Container center,Container buttons) {
        transformationListContainer.remove(centerPanel);
        transformationListContainer.remove(buttonpanel);
        this.centerPanel = center;
        this.buttonpanel = buttons;
        transformationListContainer.add(center,BorderLayout.CENTER);
        transformationListContainer.add(buttons,BorderLayout.SOUTH);
        transformationListContainer.revalidate();
    }

    public void setTransformationPanel(Container panel){

        transformationListContainer.remove(transformationPanel);
        this.transformationPanel = panel;
        transformationListContainer.add(transformationPanel,BorderLayout.EAST);
        transformationListContainer.revalidate();

    }



    public DefaultListModel getTransformationList() {
        return transformationList;
    }

    public Container getContainer() {
        return transformationListContainer;
    }

    public Container getTransformationListContainer() {
        return transformationListContainer;
    }


    public Container getTransformationPanel() {
        return transformationPanel;
    }









}
