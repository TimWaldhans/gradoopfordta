package GUI.Controller.Schedule;

import GUI.Controller.Components.ButtonComponents;
import GUI.Controller.Components.PanelComponents;
import GUI.Render;
import GraphData.DatabaseConnection.Neo4jReader;
import Transformations.CustomTransformations.BasicTransformation;
import Transformations.GradoopTransformations.AdvancedTransformation;
import Transformations.PluginTransformation;
import Transformations.Transformation;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.Serializable;


/**
 * This class represents a Transformation Data adapter for the GUI components
 */

public class TransformationElement implements Serializable{


    /**
     * GUI and controller elements
     */


    // Underlying transformation object
    private Transformation transformation;
    // Underlying associated transformation schedule
    private TransformationScheduleListElement schedule;
    // GUI elements
    private String name;
    private Container container;
    private JLabel match;
    private JLabel construct;
    private JLabel result;
    private JTextArea resultRepresentation;
    private String matchString;
    private String constructionString;
    private TextArea matchPattern;
    private TextArea constructionPattern;
    private JButton saveGraph;
    private JScrollPane graphRepPanel;
    private static final long serialVersionUID = 0000000000000000003L;



    /**
     * Constructor with associated Data
     * @param transformation Transformation Object for execution on the Gradoop object
     * @param name Name for this transformation
     * @param schedule
     */

    public TransformationElement(Transformation transformation, String name, TransformationScheduleListElement schedule){
       this.transformation = transformation;
       this.name = name;

       this.schedule = schedule;
       this.container = new Container();
       this.resultRepresentation = new JTextArea();
    }


    /**
     * Renders the center work panel representation for this object
     * @return The associated container
     */

    public Container render(){


        if(!(transformation instanceof PluginTransformation)){

            return renderForQuerybasedtrafo();
        }

        return renderForPluginTrafo();


    }


    /**
     * Renders this panel for the fitting associated Transformation  (Advanced and basic Transformations)
     * @return rendered Panel Component
     */


    private Container renderForQuerybasedtrafo(){

        container = new Container();


            BorderLayout layout = new BorderLayout();
            container.setLayout(layout);


            String transactionString = "<html> <p style=\" font-size:13px\">" +name+ "</p>" +
                    transformation.getClass().getSimpleName()+ "</html>";
            JLabel transactionStats = new JLabel(transactionString);


            Container patternBar = new Container();
            patternBar.setLayout(new FlowLayout());


            Container matchDef = new Container();
            matchDef.setLayout(new BorderLayout());
            if(transformation instanceof BasicTransformation) {
                String text1 = "<html>Please enter GDL match string:<br/>" +
                        "<p style=\" font-size:7px\">This GDL match is called on the graph passed by" +
                        " previous transformation.<br/> The GDL string on the right is then called on it." +
                        "</p>" +
                        "</html>";
                match = new JLabel(text1);
                String text2 = "<html>Please enter GDL Construction string:<br/>" +
                        "<p style=\" font-size:7px\">This GDL construction pattern is called on the result of this" +
                        " transformation <br/> and its result is passed to the next transformation " +
                        "in the schedule</p>" +
                        "</html>";
                construct = new JLabel(text2);
            }
            if(transformation instanceof AdvancedTransformation) {
                String text1 = "<html>Please enter GDL match string:<br/>" +
                        "<p style=\" font-size:7px\">This GDL match is called on the graph passed by" +
                        " previous transformation.<br/> The Gradoop functions defined on the right are then " +
                        "called on it" +
                        "</p>" +
                        "</html>";
                match = new JLabel(text1);
                String text2 = "<html>Please enter Gradoop Functions to be called:<br/>" +
                        "<p style=\" font-size:7px\">The Gradoop functionalities defined here" +
                        " are called on the matched Graph <br/>" +
                        "and its result is passed to the next transformation " +
                        "in the schedule</p>" +
                        "</html>";
                construct = new JLabel(text2);
            }

            if(matchPattern == null) {
                matchPattern = new TextArea();
            }
            matchPattern.addTextListener(new TextListener() {
            @Override
            public void textValueChanged(TextEvent textEvent) {
                matchString = matchPattern.getText();
            }
            });
            matchPattern.setPreferredSize(new Dimension(450,170));
            matchPattern.setMaximumSize(new Dimension(450,170));
            matchDef.add(match,BorderLayout.NORTH);
            matchDef.add(matchPattern,BorderLayout.CENTER);
            matchPattern.setEditable(true);

            Container constDef = new Container();
            constDef.setLayout(new BorderLayout());


            if(matchPattern == null) {
            matchPattern = new TextArea();
             }
            if(constructionPattern == null) {
                constructionPattern = new TextArea();
                constructionPattern.setSize(100,100);
            }
            constructionPattern.addTextListener(new TextListener() {
            @Override
            public void textValueChanged(TextEvent textEvent) {
                constructionString = constructionPattern.getText();
            }
            });
            constructionPattern.setPreferredSize(new Dimension(450,170));
            constructionPattern.setMaximumSize(new Dimension(450,170));
            constDef.add(construct,BorderLayout.NORTH);
            constDef.add(constructionPattern,BorderLayout.CENTER);

            patternBar.add(matchDef);
            patternBar.add(constDef);

            this.saveGraph = ButtonComponents.saveGraphButton(Render.getCenterPanel(),transformation.getResultGraph());
            saveGraph.setText("<html>Save transformation result graph </html>");
            patternBar.add(saveGraph);

            Container resultInfo = new Container();
            resultInfo.setLayout(new BorderLayout());
            result = new JLabel("Result Graph: ");
            if(transformation.getResultGraph()==null) {
                resultRepresentation = new JTextArea();

            }else{
                try {

                    resultRepresentation.setText(transformation.getResultAsString());
                }catch(Exception e){
                    PanelComponents.callInformation(e.getMessage());
                }

            }
            resultRepresentation.setEditable(false);
            graphRepPanel= new JScrollPane(resultRepresentation);
            graphRepPanel.setPreferredSize(new Dimension(200,150));
            graphRepPanel.setMaximumSize(new Dimension(300,150));
            resultInfo.add(result,BorderLayout.NORTH);
            resultInfo.add(graphRepPanel,BorderLayout.CENTER);



            container.add(transactionStats,BorderLayout.NORTH);
            container.add(patternBar,BorderLayout.CENTER);

            container.add(resultInfo,BorderLayout.SOUTH);



        return container;

    }

    /**
     * Renders a panel for a plugin based transformation
     * @return
     */


     private Container renderForPluginTrafo(){

        container = new Container();

         BorderLayout layout = new BorderLayout();
         container.setLayout(layout);


         String transactionString = "<html>  <p style=\" font-size:13px\">" +name+ "</p>" +
                 transformation.getClass().getName()+ "<br/> </html>";

         JLabel transactionStats = new JLabel(transactionString);


         Container patternBar = new Container();
         patternBar.setLayout(new FlowLayout());

         Container matchDef = new Container();
         matchDef.setLayout(new BorderLayout());
         if(transformation instanceof PluginTransformation) {
             match = new JLabel("<html>Description of this transformation: <br/>" +
                     "<p style=\" font-size:8px\"> description as passed by <br/>programmer</p></html>");
             String text = "<html>Please enter GDL match string:<br/>" +
                     "<p style=\" font-size:8px\">This GDL match is called on the result of this" +
                     "transformation <br/> and its result is passed to the next transformation " +
                     "in the schedule</p>" +
                     "</html>";
             construct = new JLabel(text);
         }

         if(matchPattern == null) {
             matchPattern = new TextArea();
         }
         PluginTransformation trafo = (PluginTransformation) transformation;
         matchPattern.setPreferredSize(new Dimension(400,150));
         matchPattern.setMaximumSize(new Dimension(400,150));
         matchPattern.setText(trafo.getDescription());
         matchDef.add(match,BorderLayout.NORTH);
         matchDef.add(matchPattern,BorderLayout.CENTER);
         matchPattern.setEditable(false);

         Container constDef = new Container();
         constDef.setLayout(new BorderLayout());

         if(constructionPattern == null) {
             constructionPattern = new TextArea();
             constructionPattern.setSize(100,100);
         }
         constructionPattern.addTextListener(new TextListener() {
             @Override
             public void textValueChanged(TextEvent textEvent) {
                 constructionString = constructionPattern.getText();
             }
         });
         constructionPattern.setPreferredSize(new Dimension(400,150));
         constructionPattern.setMaximumSize(new Dimension(400,150));
         constDef.add(construct,BorderLayout.NORTH);
         constDef.add(constructionPattern,BorderLayout.CENTER);

         patternBar.add(matchDef);
         patternBar.add(constDef);

         this.saveGraph = ButtonComponents.saveGraphButton(Render.getCenterPanel(),transformation.getResultGraph());
         saveGraph.setText("<html>Save transformation Graph </html>");
         patternBar.add(saveGraph);

         Container resultInfo = new Container();
         resultInfo.setLayout(new BorderLayout());
         result = new JLabel("Result Graph: ");
         if(transformation.getResultGraph()==null) {
             resultRepresentation = new JTextArea();

         }else{
             try {
                 resultRepresentation.setText(transformation.getResultAsString());
             }catch(Exception e){
                 PanelComponents.callInformation(e.getMessage());
             }

         }
         resultRepresentation.setEditable(false);
         graphRepPanel= new JScrollPane(resultRepresentation);
         graphRepPanel.setPreferredSize(new Dimension(200,200));
         graphRepPanel.setMaximumSize(new Dimension(300,200));
         resultInfo.add(result,BorderLayout.NORTH);
         resultInfo.add(graphRepPanel,BorderLayout.CENTER);


         container.add(transactionStats,BorderLayout.NORTH);
         container.add(patternBar,BorderLayout.CENTER);

         container.add(resultInfo,BorderLayout.SOUTH);



         return container;


    }


    // miscellaneous

    @Override
    public String toString(){
        if(name.replace(" ","").length()<=1){
            name = "<br/>    ";
        }
        if(transformation instanceof BasicTransformation){
            return "<html> Basic Transformation <br/> "+name+"</html>";
        }
        if(transformation instanceof AdvancedTransformation){
            return "<html> Advanced Transformation <br/> "+name+"</html>";
        }
        if(transformation instanceof PluginTransformation){
            return "<html> Plugin Transformation <br/> "+name+"</html>";
        }

        return name;

    }


    public String getConstructionString() {
        return constructionString;
    }

    public String getMatchPattern() {
        return matchString;
    }


    public LogicalGraph getResultGraph(){
        return transformation.getResultGraph();
    }

    public LogicalGraph executeTransformation(LogicalGraph input ){
        if(transformation instanceof BasicTransformation){
            transformation = new BasicTransformation(matchString,constructionString);

            resultRepresentation.updateUI();
            LogicalGraph result =  transformation.execute(input);
            this.resultRepresentation.setText(transformation.getResultAsString());
            return result;
        }

        if(transformation instanceof AdvancedTransformation){

            transformation = new AdvancedTransformation(matchString,constructionString);

            LogicalGraph result = transformation.execute(input);
            this.resultRepresentation.setText(transformation.getResultAsString());

            resultRepresentation.updateUI();
            return result;
        }
        if(transformation instanceof PluginTransformation){




                ((PluginTransformation) transformation).setContinuationPattern(constructionString);

                LogicalGraph result = transformation.execute(input);
                result = transformation.getResultGraph();

                resultRepresentation.setText(transformation.getResultAsString());
                resultRepresentation.updateUI();
                return result;



        }


        return null;
    }

    public Transformation getTransformation() {
        return transformation;
    }

    public String getName() {
        return name;
    }


    public void setMatchPattern(String pattern){
        this.matchPattern = new TextArea(pattern);
        this.matchString = pattern;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setConstructionPattern(String pattern){

        this.constructionPattern = new TextArea(pattern);
        this.constructionString = pattern;
    }

    public void setTransformation(Transformation transformation) {
        this.transformation = transformation;
    }
}
