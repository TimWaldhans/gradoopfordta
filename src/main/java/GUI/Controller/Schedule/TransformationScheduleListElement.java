package GUI.Controller.Schedule;

import GUI.Controller.Components.ButtonComponents;
import GUI.Controller.Components.ListComponents;
import GUI.Controller.Dialogs.SetTrafoNameDialog;
import GUI.Render;
import Transformations.TransformationAlgorithms.TransformationSchedule;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;


/**
 * This class implements a Controller for the GUI to handle and reperesent
 * a set of transformations on a Neo4j Database
 */


public class TransformationScheduleListElement implements Serializable{



    // underlying schedule Object
    private TransformationSchedule schedule;
    // underlying transformation adapter
    private DefaultListModel<TransformationElement> transformationList;
    // GUI components
    private JLabel name;
    private String nameString;
    private String URI;
    private String username;
    private String pw;
    private JList transformations;
    private TextArea queryText;
    private   JTextArea graphRepresentation;
    private  JLabel caption;
    private JLabel stats;
    private  JScrollPane graphRepPanel;
    private  JButton loadGraph;
    private JButton saveGraph;
    private  JButton runTrafos;
    private  JButton commit;
    private  JButton importGraph;
    private  Container buttoncontainer;
    private static final long serialVersionUID = 0000000000000000004L;

    // indicates wether this schedule is on a local database
    private boolean local;
    // index in List representation
    private int index;


    /**
     * Creates a schedule adapter Object for a local database
     * @param name The name of this transformation
     * @param path The path to the database
     * @param index The index in the List representation
     */

    public TransformationScheduleListElement(String name, String path, int index) {
        local = true;
        this.name = new JLabel(name);
        this.URI = path;
        this.index = index;
        this.schedule = new TransformationSchedule("",path);
        this.transformationList = new DefaultListModel();
        this.queryText = new TextArea();
        transformations = new JList(transformationList);

    }

    /**
     * Constructor for copying
     * @param another TransformationScheduleListElement to copy
     */

    public TransformationScheduleListElement(TransformationScheduleListElement another){
        this.schedule = another.getSchedule();
        this.transformationList = another.getTransformationList() ;
        this.transformations = another.getTransformations();
        if(another.isLocal()){
            this.name = another.getName();
            this.URI = another.getURI();
            this.index = another.getIndex() +1;
            this.local = true;
        }else{
            this.name = another.getName();
            this.URI = another.getURI();
            this.username = another.getUsername();
            this.pw = another.getPw();
            this.index = another.getIndex() +1;
            this.local = false;
        }
    }

    /**
     * Creates a schedule adapter Object for a remote database
     * @param name The name of this transformation
     * @param uri The URI for the bolt protocol
     * @param username The login username
     * @param pw       The login password
     * @param index    The index in the List representation
     */

    public TransformationScheduleListElement(String name, String uri, String username, String pw, int index) {
        local = false;
        this.name = new JLabel(name);
        this.index = index;
        this.URI = uri;
        this.username = username;
        this.pw = pw;
        this.schedule = new TransformationSchedule("",uri,username,pw);
        this.transformationList = new DefaultListModel();
        this.queryText = new TextArea();
        transformations = new JList(transformationList);
    }


    /**
     * Renders a work Panel for this schedule for the GUI
     */

    public  void  render(){
        Container container = new Container();
        BorderLayout mainLayout = new BorderLayout();
        container.setLayout(mainLayout);




        // sets or generates  the query text of this Schedule

            queryText = new TextArea(schedule.getCypherBaseQuery());
            queryText.setSize(100, 100);
            queryText.addTextListener(new TextListener() {
                @Override
                public void textValueChanged(TextEvent textEvent) {
                    schedule.setCypherBaseQuery(queryText.getText());

                }
            });

            queryText.setRows(40);

        stats = new JLabel();


        // Creates the String representation Components of a loaded Graph
        graphRepresentation = new JTextArea(schedule.getString());
        graphRepresentation.setEditable(false);



        graphRepPanel= new JScrollPane(graphRepresentation);
        graphRepPanel.setPreferredSize(new Dimension(200,200));
        graphRepPanel.setMaximumSize(new Dimension(300,200));

        if(schedule.getBaseGraph() != null){
            graphRepresentation.setText(schedule.getString());
        }




        // loads Textpanels and Buttons
        JPanel queryTextPanel = new JPanel();
        BorderLayout borderlayout = new BorderLayout();
        queryTextPanel.setLayout(borderlayout);
        queryTextPanel.setPreferredSize(new Dimension(500,350));
        queryTextPanel.setMaximumSize(new Dimension(500,350));
        queryTextPanel.add(queryText,BorderLayout.CENTER);
        String match = "Pattern to extract from Neo4j Database: ";
        JLabel matchdes = new JLabel(match);
        queryTextPanel.add(matchdes,BorderLayout.NORTH);
        loadGraph = ButtonComponents.loadGraphButton(this,queryText,schedule,graphRepresentation,
                graphRepPanel);
        runTrafos =  ButtonComponents.loadRunScheduleButton(transformationList,schedule);
        commit =   ButtonComponents.loadCommitButton(schedule);
        Container buttons = new Container();
        FlowLayout layout = new FlowLayout();
        buttons.setLayout(layout);
        buttons.add(loadGraph);
        buttons.add(runTrafos);
        buttons.add(commit);
        queryTextPanel.add(buttons,BorderLayout.SOUTH);

        // Name and credentials Text of this
        String statsFomattedText = "";
        if(schedule.getBaseGraph() == null) {
             statsFomattedText = "<html> " + "<p style=\" font-size:13px\">"+ " " +getNameAsString()+"</p>" +
                    "" + getUsername() +
                    " @ " + getURI() + "</html>";


        }else{

             statsFomattedText = "<html> " + "<p style=\" font-size:13px\">"+" "+getNameAsString() + "</p>" +
                    "" + getUsername() +
                    " @ " + getURI() + "</html>";

            LogicalGraph baseGraph = schedule.getBaseGraph();
            try {
                long nodeCount = baseGraph.getVertices().count();
                long edgeCount = baseGraph.getEdges().count();

                statsFomattedText = "<html> " + "<p style=\" font-size:13px\">"+ " "+getNameAsString() + "</p>"  +
                        "" + getUsername() +
                        " @ " + getURI() + " | Node count: " + nodeCount +" | Edge count: " + edgeCount + "</html>";
            }catch(Exception e){
                e.printStackTrace();
            }

        }  stats.setText(statsFomattedText);



        container.add(stats,BorderLayout.NORTH);
        container.add(queryTextPanel,BorderLayout.CENTER);;
        container.add(graphRepPanel,BorderLayout.SOUTH);


        Render.getCenterPanel().setCenterPanel(container,renderButtonPanel());
        Render.getCenterPanel().setTransformationPanel(renderTransformationList());


    }

    /**
     * Renders a buttonpanel with functionalities associated with this schedule
     * @return Container with the Buttons
     * @see ButtonComponents
     */

    private Container renderButtonPanel(){

        buttoncontainer = new Container();
        FlowLayout flowLayout = new FlowLayout(3);
        buttoncontainer.setLayout(flowLayout);
        buttoncontainer.add(ButtonComponents.loadAddTransformationButton(Render.getCenterPanel(),
                transformationList,this ,transformations));
        buttoncontainer.add(ButtonComponents.loadCleanButton());
        saveGraph = ButtonComponents.saveGraphButton(Render.getCenterPanel(),schedule.getBaseGraph());
        importGraph = ButtonComponents.importGraphButton(Render.getCenterPanel(),this);
        buttoncontainer.add(importGraph);
        buttoncontainer.add(saveGraph);

        return buttoncontainer;

    }


    /**
     * Renders the List of transformations associated with this schedule
     *
     * @return Container with the List data and adapter
     * @see TransformationElement
     */

    private Container renderTransformationList(){

        Container container = new Container();
        container.setLayout(new BorderLayout());
        String label = "<html>Transformations associated  <br/> with this schedule <br/> </html>";
        this.caption = new JLabel(label);
        transformations =   ListComponents.colorRender(transformations);
        JScrollPane scrollableList = new JScrollPane(transformations);
        container.add(scrollableList,BorderLayout.CENTER);
        container.add(caption,BorderLayout.NORTH);


        final JPopupMenu popupMenu = new JPopupMenu();

        JMenuItem rename = new JMenuItem("Rename");
        popupMenu.add(rename);
        rename.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformations.getSelectedIndex();
                JDialog diag = new SetTrafoNameDialog(Render.getFrames()[0],
                        transformationList.get(selectedIndex),transformations);
                diag.setVisible(true);
                transformationList.get(selectedIndex).render();
            }
        });


        JMenuItem delete = new JMenuItem("Delete");
        popupMenu.add(delete);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformations.getSelectedIndex();
                if(transformationList.size() == 1){
                    transformationList.removeAllElements();
                    if(transformationList.size()>0) {
                        transformationList.remove(selectedIndex);
                    }
                    render();
                    transformations.setModel(transformationList);
                    transformations.updateUI();
                }else {
                    transformationList.remove(selectedIndex);
                    transformations.revalidate();
                    transformations.updateUI();
                    transformationList.get(selectedIndex-1).render();
                    if(schedule.getTransformations().size()!=0) {
                        schedule.getTransformations().remove(selectedIndex);
                    }
                    render();
                }
            }
        });

        JMenuItem moveUp = new JMenuItem("Move Up");
        popupMenu.add(moveUp);
        moveUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformations.getSelectedIndex();
                if(selectedIndex==0){
                    return;
                }
                TransformationElement tmp = transformationList.get(selectedIndex-1);
                transformationList.set(selectedIndex-1,transformationList.get(selectedIndex));
                transformationList.set(selectedIndex,tmp);

                    transformations.updateUI();

            }
        });
        JMenuItem moveDown = new JMenuItem("Move Down");
        popupMenu.add(moveDown);
        moveDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int selectedIndex = transformations.getSelectedIndex();
                if(selectedIndex== transformationList.size() -1){
                    return;
                }
                TransformationElement tmp = transformationList.get(selectedIndex+1);
                transformationList.set(selectedIndex+1,transformationList.get(selectedIndex));
                transformationList.set(selectedIndex,tmp);

                transformations.updateUI();

            }
        });


        transformations.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isLeftMouseButton(me)) {

                    if(transformationList.size()==0){
                        return;
                    }
                    TransformationElement element = (TransformationElement) transformations.getSelectedValue();
                    Render.getCenterPanel().setVisible(false);
                    Container transactionPanel = element.render();

                    transactionPanel.setVisible(true);
                    Render.getCenterPanel().setCenterPanel(transactionPanel,renderButtonPanel());

                }
                if(SwingUtilities.isRightMouseButton(me)){
                    if (!transformations.isSelectionEmpty()
                            && transformations.locationToIndex(me.getPoint())
                            == transformations.getSelectedIndex()) {

                        popupMenu.show(transformations, me.getX(), me.getY());
                    }
                }
            }
        });



        return container;


    }





    // gettes and setters



    @Override
    public String toString(){

        String nameString = name.getText();
        if(nameString.length()<=1){
            nameString = "Schedule";
        }
        String representation = "<html>"+
                "<b>"+nameString+"</b> <br/> </body></html>";

        return representation;
    }

    public TransformationSchedule getTransformationSchedule() {
        return schedule;
    }

    public String getURI() {
        return URI;
    }

    public String getUsername() {
        return username;
    }

    public boolean isLocal() {
        return local;
    }

    public String getNameAsString(){
        return this.name.getText();
    }

    public void purge(){

       this.transformationList = null;
       this.schedule = null;
    }

    public void setName(JLabel name) {
        this.name = name;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public DefaultListModel<TransformationElement> getTransformationList() {
        return transformationList;
    }



    public JTextArea getGraphRepresentation() {
        return graphRepresentation;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getGraphGDL(){
        if(schedule.getBaseGraph() == null){
            return "";
        }
        try {
            LogicalGraph lg = schedule.getBaseGraph();
            GDLEncoder enc = new GDLEncoder(lg.getGraphHead().collect(),
                    lg.getVertices().collect(), lg.getEdges().collect());

            return enc.getGDLString();

        }catch(Exception e){
            e.printStackTrace();
        }

        return "";

    }

    public void setQueryText(String s) {
       schedule.setCypherBaseQuery(s);
    }

    public TransformationSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(TransformationSchedule schedule) {
        this.schedule = schedule;
    }

    public void setTransformationList(DefaultListModel<TransformationElement> transformationList) {
        this.transformationList = transformationList;
    }

    public JLabel getName() {
        return name;
    }

    public JList getTransformations() {
        return transformations;
    }

    public void setTransformations(JList transformations) {
        this.transformations = transformations;
    }

    public void setQueryText(TextArea queryText) {
        this.queryText = queryText;
    }

    public void setGraphRepresentation(JTextArea graphRepresentation) {
        this.graphRepresentation = graphRepresentation;
    }

    public JLabel getCaption() {
        return caption;
    }

    public void setCaption(JLabel caption) {
        this.caption = caption;
    }

    public JLabel getStats() {
        return stats;
    }

    public void setStats(JLabel stats) {
        this.stats = stats;
    }

    public JScrollPane getGraphRepPanel() {
        return graphRepPanel;
    }

    public void setGraphRepPanel(JScrollPane graphRepPanel) {
        this.graphRepPanel = graphRepPanel;
    }

    public JButton getLoadGraph() {
        return loadGraph;
    }

    public void setLoadGraph(JButton loadGraph) {
        this.loadGraph = loadGraph;
    }

    public JButton getSaveGraph() {
        return saveGraph;
    }

    public void setSaveGraph(JButton saveGraph) {
        this.saveGraph = saveGraph;
    }

    public JButton getRunTrafos() {
        return runTrafos;
    }

    public void setRunTrafos(JButton runTrafos) {
        this.runTrafos = runTrafos;
    }

    public JButton getCommit() {
        return commit;
    }

    public void setCommit(JButton commit) {
        this.commit = commit;
    }

    public JButton getImportGraph() {
        return importGraph;
    }

    public void setImportGraph(JButton importGraph) {
        this.importGraph = importGraph;
    }

    public Container getButtoncontainer() {
        return buttoncontainer;
    }

    public void setButtoncontainer(Container buttoncontainer) {
        this.buttoncontainer = buttoncontainer;
    }

    public int getIndex() {
        return index;
    }

    public TextArea getQueryText() {
       return this.queryText;
    }
}
