import GUI.Render;
import GUI.Controller.Schedule.TransformationElement;
import GUI.Controller.Schedule.TransformationScheduleListElement;
import GraphData.GraphFileHandler.GraphWriter;
import GraphData.Serialization.SerializationFileHandler;
import javax.swing.DefaultListModel;


/**
 * Main class for managing commandline parameters
 *
 */
public class main {


    // Command line Options and helptexts
    private static String pathToSchedule = "";
    private static boolean saveResultGraphs = false;
    private static boolean saveAllGraphs = false;
    private static boolean savePartialGraphs = false;
    private static String pathToSaveGraphs = "";
    private static boolean commit = false;
    private static boolean execute = false;
    private static final String helptext =  "\n"+
            "************************************************************\n"+
            "Interface to apply Gradoop functionalities on Neo4j Databases\n"+
            "Gradoop:          https://github.com/dbs-leipzig/gradoop \n"+
            "Neo4j:            https://neo4j.com/developer/ \n"+
            "Find Repository:  https://TimWaldhans@bitbucket.org/TimWaldhans/gradoopfordta.git \n"+
            "Developer API:    https://TimWaldhans@bitbucket.org/TimWaldhans/gradoopinterface.git\n"+
            "\n"+
            "Command Line Arguments:\n"+
            "-G:        Starts Graphical User Interface\n"+
            "-X:        Executes Transformations from .ser file, Enter path with quotes after arguments\n"+
            "-S:        Saves Graphs of transformations into .gdl file \n" +
            "           Works with -all, -results, -partial \n" +
            "           Enter path with quotes after arguments and path to .ser file\n"+
            "           -results: Saves inital and finals graphs from Transformation Schedules to path \n"+
            "           -partial: Saves only graphs of the Transformations themselves of the schedules \n"+
            "           -all:     Does both, -partial and -result\n"+
            "-C         Commits transformation results to Neo4j Database\n \n"+
            "Example:\n"+
            "-X -S -C -all \"/user/transformation.ser\" \"user/resultgraphs/\" \n"+
            "*************************************************************";

    public static void main(String[] args) {
        boolean guiStarted = false;

        for(int i = 0; i < args.length; i++) {
            String s = args[i];
            if (s.toLowerCase().equals("-g") && !guiStarted) {
                Render.main(null);
                guiStarted = true;
                System.out.println("GUI started");
            }

            String arg = s.toLowerCase();
            switch(arg) {
                case "-help":
                case "help":
                case "man":
                    System.out.println(helptext);
                    break;
                case "-x":
                     execute = true;
                     break;
                case "-c":
                    commit = true;
                    break;
                case "-s":
                    switch (args[i+1].toLowerCase()){
                        case "-all":
                            saveAllGraphs = true;
                            break;
                        case "-results":
                            saveResultGraphs = true;
                            break;
                        case "-partial":
                            savePartialGraphs = true;
                            break;
                    }
                    pathToSaveGraphs = args[args.length-1];

            }

        }

        if (execute) {
            System.out.println("Running Transformations...");
            if (saveAllGraphs || savePartialGraphs || saveResultGraphs) {
                pathToSchedule = args[args.length - 2];
                runTrafos();
            } else {
                pathToSchedule = args[args.length - 1];
                runTrafos();
            }
        }

        if (saveAllGraphs) {
            System.out.println("Saving All Transformation Graphs...");
            saveAllGraphs();
        }

        if (saveResultGraphs) {
            System.out.println("Saving Result Graphs...");
            saveResultGraphs();
        }

        if (savePartialGraphs) {
            System.out.println("Saving Partial Graphs...");
            savePartialGraphs();
        }

        if (commit) {
            System.out.println("Commiting...");
            commitTrafos();
        }

    }


    /**
     * Runs transformations associated with a loaded schedule
     * Controlled by command line parameters
     */
    private static void runTrafos() {
        Render.setTransformationList(new DefaultListModel());
        SerializationFileHandler.ImportSchedules(pathToSchedule);

        for(int i = 0; i < Render.getTransformationList().size(); ++i) {
            TransformationScheduleListElement element = (TransformationScheduleListElement)Render.getTransformationList().get(i);

            for(int j = 0; j < element.getTransformationList().size(); ++j) {
                element.getSchedule().addGrafo((TransformationElement)element.getTransformationList().get(j));
            }

            if (element.isLocal()) {
                element.getSchedule().loadGraphLocally();
            }

            if (!element.isLocal()) {
                element.getSchedule().loadGraphRemotely();
            }

            System.out.println("Running" + element.getName().getText());
            element.getSchedule().run();

        }

    }

    /**
     * If argument is set, commits the
     * result of a loaded  and executed schedule.
     */
    private static void commitTrafos() {
        SerializationFileHandler.ImportSchedules(pathToSchedule);

        for(int i = 0; i < Render.getTransformationList().size(); ++i) {
            TransformationScheduleListElement element = (TransformationScheduleListElement)Render.getTransformationList().get(i);
            element.getSchedule().commit();
        }

    }

    /**
     * If argument is set, saves all Graphs of associated schedule
     * into passed path as a .gdl file
     */
    private static void saveAllGraphs() {
        saveResultGraphs();
        savePartialGraphs();
    }


    /**
     * If argument is set, saves the input and result graph of a associated schedule
     * into passed path as a .gdl file
     */

    private static void saveResultGraphs() {
        for(int i = 0; i < Render.getTransformationList().size(); ++i) {
            TransformationScheduleListElement element = (TransformationScheduleListElement)Render.getTransformationList().get(i);
            GraphWriter.writeGraphGDL(element.getSchedule().getfinalGraph(), pathToSaveGraphs, element.getName().getText() + "_final");
            GraphWriter.writeGraphGDL(element.getSchedule().getBaseGraph(), pathToSaveGraphs, element.getName().getText() + "_initial");
        }

    }

    /**
     * If argument is set, saves single transformations result Graphs of a associated schedule
     * into passed path as a .gdl file
     */

    private static void savePartialGraphs() {
        for(int i = 0; i < Render.getTransformationList().size(); ++i) {
            TransformationScheduleListElement element = (TransformationScheduleListElement)Render.getTransformationList().get(i);

            for(int j = 0; j < element.getTransformationList().size() ; ++j) {
                TransformationElement trafo = (TransformationElement)element.getTransformationList().get(j);
                GraphWriter.writeGraphGDL(trafo.getResultGraph(), pathToSaveGraphs, element.getName().getText() + "_" + trafo.getName());
            }
        }

    }
}

