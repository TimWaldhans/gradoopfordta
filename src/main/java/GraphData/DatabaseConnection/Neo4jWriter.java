package GraphData.DatabaseConnection;


import GUI.Controller.Components.PanelComponents;
import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Local.LocalWriter;
import Interface.Neo4j.Remote.RemoteWriter;
import Interface.Neo4j.Writer;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * This class is a Interface for the GUI for writing Graphs into
 *  a Neo4j database out of the Gradoop format
 */
public class Neo4jWriter {



    // Underlying GradoopConfiguration, URI OR Path to connect to plus
    // the Implementation of the Neo4j to Gradoop interface
    private GradoopConfig config;
    private String URI;
    private Writer writer;


    /**
     * Constructor for a Local Neo4j database Directory
     * @param path Path to a local Neo4j Database Directory
     * @see GradoopConfig
     * @see LocalWriter
     */


    public Neo4jWriter(String path){
        this.URI = URI;
        this.config = new GradoopConfig();
        this.writer = new LocalWriter(URI);

    }

    /**
     * Constructor for a Remote Neo4j Database
     * @param Uri Bolt protocol URI for the neo4j database
     * @param User Username for login
     * @param password password for Login
     * @see RemoteWriter
     */
    public Neo4jWriter(String Uri,String User, String password){
        this.URI = URI;
        this.config = new GradoopConfig();
        this.writer = new RemoteWriter(Uri,User,password);

    }


    /**
     * Writes the Graph to the connected database, depending on the changes
     * which are detected between the first and second Graph
     * @param first Pre- transformation Graph
     * @param second Post- transformation Graph
     */

    public void putGraph(LogicalGraph first, LogicalGraph second){
        try{
            writer.putGraph(first,second);
        }catch(Exception e){
            PanelComponents.callInformation(e.getMessage());
        }
    }

    public GradoopConfig getConfig() {
        return config;
    }

    public String getURI() {
        return URI;
    }

    public void close(){
        this.writer.close();
    }

    public Writer getWriter() {
        return writer;
    }
}
