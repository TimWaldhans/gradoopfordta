package GraphData.Serialization;

import GUI.Controller.Components.PanelComponents;
import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Render;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;


public class SerializationFileHandler {


    /**
     * Exports a Transformation Schedule as a Java serializable Object to the
     * given path
     * @param path The path to save
     * @param name The Filmename
     */


    public static void exportSchedules(String path, String name){


        DefaultListModel<TransformationScheduleListElement> transactionList = Render.getTransformationList();

        PanelComponents.callInformation("Exporting " + transactionList.size() + "Transformation(s) to " + path);

        List<ScheduleSerializable> scheduleSerialization = new LinkedList<>();
        for(int i = 0; i< transactionList.size();i++){
            scheduleSerialization.add(new ScheduleSerializable(transactionList.get(i)));
        }

        try{

            FileOutputStream fos = new FileOutputStream (path+name+".ser");
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(scheduleSerialization);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    /**
     * Imports a Transformation Schedule into the environment
     * given path
     * @param path The path to save
     */


    public static void ImportSchedules(String path){

        try{
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            List<ScheduleSerializable> schedule = (List) ois.readObject();

            for(ScheduleSerializable scheduleSer : schedule ){
                Render.getTransformationList().addElement(createFromSerialSchedule(scheduleSer));


            }

        }catch(Exception e){
            e.printStackTrace();
        }



    }


    /**
     * Converts the serializable element into the Data representation for runtime
     * @param ser Serialization Object for schedules
     * @return The converted Schedule
     */

    private static TransformationScheduleListElement createFromSerialSchedule(ScheduleSerializable ser){

        int listsize = Render.getTransformationList().size();
        TransformationScheduleListElement result;


        if(ser.isLocal()) {

            result = new TransformationScheduleListElement(ser.getName(),ser.getURI(),listsize++);

            result.setQueryText(ser.getQueryText());


        }else{

            result = new TransformationScheduleListElement(ser.getName(),ser.getURI(),ser.getUser(),ser.getPw(),
                    listsize++);

            result.setQueryText(ser.getQueryText());


        }
        LinkedList<TransformationSerializable> transformations = ser.getTransformations();

        for(TransformationSerializable transformationSerializable : transformations){

            result.getTransformationList().addElement(transformationSerializable.getTrafo(result));
            result.getTransformations().revalidate();



        }


        return result;



    }




}
