package GraphData.Serialization;

import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Controller.Schedule.TransformationElement;
import Transformations.CustomTransformations.BasicTransformation;
import Transformations.GradoopTransformations.AdvancedTransformation;
import Transformations.PluginTransformation;

import java.io.Serializable;


/**
 * This class implements the serializable representation of a single Graph transformation
 */

public class TransformationSerializable implements Serializable {




    // credentials
    private String name;
    private String matchPattern;
    private String constructionPattern;
    private String type;
    // associated schedule
    private PluginTransformation trafo;
    private static final long serialVersionUID = 0000000000000000002L;


    /**
     * Constructor which convertes the given Transformation elements for serialization
     * @param transformation Associated transformation
     */

    public TransformationSerializable(TransformationElement transformation){

        this.name = transformation.getName();
        this.matchPattern = transformation.getMatchPattern();
        this.constructionPattern = transformation.getConstructionString();
        if(transformation.getTransformation() instanceof BasicTransformation){
            type = "gradoopCustom";

        }
        if(transformation.getTransformation() instanceof AdvancedTransformation){
            type = "gradoop";

        }
        if(transformation.getTransformation() instanceof PluginTransformation){
            type = "plugin";
            trafo = (PluginTransformation) transformation.getTransformation();
        }



    }


    /**
     * Converts this serializable object back to the original data adapter
     * @return The Transformation for the environment
     */
    public TransformationElement getTrafo(TransformationScheduleListElement element){

        TransformationElement result = new TransformationElement(null,"",null);
        if(type.equals("gradoopCustom")){
            BasicTransformation trafo = new BasicTransformation(matchPattern,constructionPattern);
            result = new TransformationElement(trafo,name,element);
            result.setConstructionPattern(constructionPattern);
            result.setMatchPattern(matchPattern);


        } if(type.equals("gradoop")){
            AdvancedTransformation trafo = new AdvancedTransformation(matchPattern,constructionPattern);
            result = new TransformationElement(trafo,name,element);
            result.setConstructionPattern(constructionPattern);
            result.setMatchPattern(matchPattern);
        }if(type.equals("plugin")){
            result.setTransformation(trafo);
            result.setConstructionPattern(constructionPattern);
        }



        return result;
    }

    public String getName() {
        return name;
    }

    public String getMatchPattern() {
        return matchPattern;
    }

    public String getConstructionPattern() {
        return constructionPattern;
    }




}
