package GraphData.Serialization;

import GUI.Controller.Schedule.TransformationScheduleListElement;
import GUI.Controller.Schedule.TransformationElement;

import javax.swing.*;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * This class implements a minimal representation for a Transformation
 * Schedule for serialization
 */

public class ScheduleSerializable implements Serializable {



     // credentials
     private String name;
     private String URI;
     private String user;
     private String path;
     private String pw;
     private String queryText;
     // List of associated transformations
     private LinkedList<TransformationSerializable> transformations;
     // indicates wether the schedule is local or remote
     private boolean local;
     private static final long serialVersionUID = 0000000000000000001L;

     /**
      * Constructor for innitiation
      * @param transformationScheduleListElement The schedule Adapter
      * @see TransformationScheduleListElement
      */

     public ScheduleSerializable (TransformationScheduleListElement transformationScheduleListElement){

          transformations = new LinkedList<TransformationSerializable>();
          this.local = transformationScheduleListElement.isLocal();
          if(local) {
               this.name = transformationScheduleListElement.getNameAsString();
               this.path = transformationScheduleListElement.getURI();
               this.queryText = transformationScheduleListElement.getQueryText().getText();


          }else{
               this.name = transformationScheduleListElement.getNameAsString();
               this.URI = transformationScheduleListElement.getURI();
               this.user = transformationScheduleListElement.getUsername();
               this.pw = transformationScheduleListElement.getPw();
               this.queryText = transformationScheduleListElement.getQueryText().getText();
          }



          int i = transformationScheduleListElement.getTransformationList().size();
          DefaultListModel<TransformationElement> transformationList = transformationScheduleListElement.
                  getTransformationList();
          for(int j = 0 ; j< i; j++){
               transformations.add(new TransformationSerializable(transformationList.get(j)));
          }



     }

     public String getName() {
          return name;
     }

     public String getURI() {
          return URI;
     }

     public String getUser() {
          return user;
     }

     public String getPath() {
          return path;
     }


     public String getPw() {
          return pw;
     }

     public String getQueryText() {
          return queryText;
     }

     public LinkedList<TransformationSerializable> getTransformations() {
          return transformations;
     }

     public boolean isLocal() {
          return local;
     }




}
